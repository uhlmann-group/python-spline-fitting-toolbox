#! /usr/bin/env python

from setuptools import setup

VERSION = "0.1.0"
AUTHOR = "Virginie Uhlmann"
AUTHOR_EMAIL = "uhlmann@ebi.ac.uk"


setup(
    name="sft",
    version=VERSION,
    description="A Python Spline Fitting Toolbox",
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    packages=[
        "sft",
    ],
    classifiers=[
        "Intended Audience :: Science/Research",
        "Intended Audience :: Developers",
        "License :: OSI Approved",
        "Programming Language :: C",
        "Programming Language :: Python",
        "Topic :: Software Development",
        "Topic :: Scientific/Engineering",
        "Operating System :: POSIX",
        "Operating System :: Unix",
        "Operating System :: MacOS",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: " "Implementation :: CPython",
    ],
    url="https://gitlab.ebi.ac.uk/uhlmann-group/python-spline-fitting-toolbox",
    python_requires=">=3.6",
)
