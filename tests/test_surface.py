#!/usr/bin/env python
# coding: utf-8

import tensorflow as tf
import os.path as osp

from skimage import io
import numpy as np
import pytest

from sft.spline.surface_models import SplineSphere
from tests import TEST_DATA


def test_parameter_to_world():
    # Define a perfect sphere
    radius = 1.0
    center = np.array([0, 0, 0])

    Mt = 5
    Ms = 5
    model = SplineSphere((Ms, Mt))
    model.initialize_sphere(radius, center)

    # Compare tangents to theoretical tangents
    t = 0.324543643
    s = 0.755343674

    ddt_true = np.array(
        [-radius * np.sin(np.pi * s) * np.sin(2.0 * np.pi * t),
         radius * np.sin(np.pi * s) * np.cos(2.0 * np.pi * t),
         0])
    dds_true = np.array(
        [radius * np.cos(np.pi * s) * np.cos(2.0 * np.pi * t),
         radius * np.cos(np.pi * s) * np.sin(2.0 * np.pi * t),
         -radius * np.sin(np.pi * s)])

    dds_comp = model.parametersToWorld((s * (Ms - 1), t * Mt), ds=True, dt=False)
    ddt_comp = model.parametersToWorld((s * (Ms - 1), t * Mt), ds=False, dt=True)

    np.testing.assert_almost_equal(dds_comp, dds_true)
    np.testing.assert_almost_equal(ddt_comp, ddt_true)


@pytest.mark.parametrize('use_tf', [True, False])
@pytest.mark.parametrize('Ms', np.arange(3, 5))
@pytest.mark.parametrize('Mt', np.arange(3, 5))
@pytest.mark.parametrize('s_rate_s', [5, 10, 20])
@pytest.mark.parametrize('s_rate_t', [5, 10, 20])
@pytest.mark.parametrize('radius', np.logspace(-6, 6, 7))
@pytest.mark.parametrize('center', [(0, 0, 0), (-12.3, 37, 1337)])
def test_initialize_from_points(use_tf, Ms, Mt, s_rate_s, s_rate_t, radius, center):

    # We sample points to recreate the sphere
    J = ((Ms - 1) * s_rate_s) + 1
    I = Mt * s_rate_t

    sample_points = np.zeros((I * J, 3))
    for j in range(0, J):
        for i in range(0, I):
            ip = i + I * j
            point = np.array([
                np.cos(2.0 * np.pi * i / I) * np.sin(np.pi * j / (J - 1)),
                np.sin(2.0 * np.pi * i / I) * np.sin(np.pi * j / (J - 1)),
                np.cos(np.pi * j / (J - 1))])
            sample_points[ip] = center + radius * point

    if use_tf:
        sample_points = tf.convert_to_tensor(sample_points)

    surface = SplineSphere(M=(Ms, Mt))
    surface.initialize_from_points(sample_points, sampling_rates=(s_rate_s, s_rate_t))

    # Initialize sphere for the given parameters
    sphere = SplineSphere(M=(Ms, Mt))
    sphere.initialize_sphere(radius=radius, center=center)

    # Check for identical internals
    np.testing.assert_almost_equal(sphere.coefs, surface.coefs)
    np.testing.assert_almost_equal(sphere.tans, surface.tans)
    np.testing.assert_almost_equal(sphere.north_pole, surface.north_pole)
    np.testing.assert_almost_equal(sphere.south_pole, surface.south_pole)

    # Check for same sampled points
    sample_points_from_sphere = sphere.sample(sampling_rates=(s_rate_s, s_rate_t))
    np.testing.assert_almost_equal(sample_points, sample_points_from_sphere)

    # Check for the internals proper types
    assert use_tf == tf.is_tensor(surface.coefs)
    assert use_tf == tf.is_tensor(surface.control_points)


@pytest.mark.parametrize('Ms', np.arange(3, 5))
@pytest.mark.parametrize('Mt', np.arange(3, 5))
@pytest.mark.parametrize('N', np.logspace(1, 4, dtype=int))
def test_sampling_N(Ms, Mt, N, radius=1337):

    # Initialize sphere for the given parameters
    sphere = SplineSphere(M=(Ms, Mt))
    sphere.initialize_sphere(radius=radius, center=(0.0, 0.0, 0.0))

    # Check for same sampled points
    sampled_points = sphere.sample(N=N)
    assert sampled_points.shape == (N, 3)

    # We assert that point are on the sphere
    np.testing.assert_almost_equal(
        np.linalg.norm(sampled_points, axis=1),
        radius * np.ones((N,)),
        decimal=6
    )


@pytest.mark.parametrize('use_tf', [True, False])
@pytest.mark.parametrize('Ms', np.arange(3, 5))
@pytest.mark.parametrize('Mt', np.arange(3, 5))
@pytest.mark.parametrize('radius', np.logspace(-6, 6, 7))
@pytest.mark.parametrize('center', [(0, 0, 0), (-12.3, 37, 1337)])
def test_initialize_from_control_points(use_tf, Ms, Mt, radius, center):
    # We sample points to recreate the sphere
    control_points = np.zeros(((Ms - 2) * Mt + 2, 3))

    PIMs = np.pi / (Ms - 1)
    PIMt = np.pi / Mt

    scaleMs = 2.0 * (1.0 - np.cos(PIMs)) / (np.cos(PIMs / 2) - np.cos(3.0 * PIMs / 2))
    scaleMt = 2.0 * (1.0 - np.cos(2.0 * PIMt)) / (np.cos(PIMt) - np.cos(3.0 * PIMt))
    for l in range(1, Ms - 1):
        for k in range(0, Mt):
            ip = k + 1 + Mt * (l - 1)
            theta = l * PIMs
            phi = 2.0 * k * PIMt
            point = np.array([
                scaleMs * scaleMt * np.cos(phi) * np.sin(theta),
                scaleMs * scaleMt * np.sin(phi) * np.sin(theta),
                scaleMs * np.cos(theta)])
            control_points[ip] = center + radius * point

    # North and south poles
    control_points[0] = center + np.array([0, 0, radius])
    control_points[-1] = center + np.array([0, 0, -radius])

    surface = SplineSphere(M=(Ms, Mt))

    if use_tf:
        control_points = tf.convert_to_tensor(control_points)

    surface.control_points = control_points

    tans = np.array([
        surface.north_pole + np.array([radius * np.pi, 0, 0]),
        surface.north_pole + np.array([0, radius * np.pi, 0]),
        surface.south_pole + np.array([radius * np.pi, 0, 0]),
        surface.south_pole + np.array([0, radius * np.pi, 0]),
    ])
    surface.tans = tans

    # Initialize sphere for the given parameters
    sphere = SplineSphere(M=(Ms, Mt))
    sphere.initialize_sphere(radius=radius, center=center)

    # Check for identical internals
    np.testing.assert_almost_equal(sphere.north_pole, surface.north_pole)
    np.testing.assert_almost_equal(sphere.south_pole, surface.south_pole)
    np.testing.assert_almost_equal(sphere.coefs, surface.coefs)
    np.testing.assert_almost_equal(sphere.tans, surface.tans)

    # Check for same sampled points
    sample_points_from_sphere = sphere.sample(sampling_rates=(10, 10))
    sample_points_from_surface = surface.sample(sampling_rates=(10, 10))
    np.testing.assert_almost_equal(sample_points_from_sphere,
                                   sample_points_from_surface)

    # Check for the internals proper types
    assert use_tf == tf.is_tensor(surface.coefs)
    assert use_tf == tf.is_tensor(surface.control_points)


@pytest.mark.parametrize('use_tf', [True, False])
@pytest.mark.parametrize('Ms', np.arange(3, 5))
@pytest.mark.parametrize('Mt', np.arange(3, 5))
@pytest.mark.parametrize('s_rate_s', [5, 10, 20])
@pytest.mark.parametrize('s_rate_t', [5, 10, 20])
@pytest.mark.skip(reason="To be sorted out")
def test_initialise_from_binary_mask(use_tf, Ms, Mt, s_rate_s, s_rate_t):

    data = io.imread(osp.join(TEST_DATA, "volume.tif"))
    if use_tf:
        data = tf.convert_to_tensor(data)

    surface = SplineSphere((Ms, Mt))
    surface.initialise_from_binary_mask(data, sampling_rate=(s_rate_s, s_rate_t))

    # Check for the internals proper types
    assert use_tf == tf.is_tensor(surface.coefs)
    assert use_tf == tf.is_tensor(surface.control_points)


@pytest.mark.skip(reason="Computing the winding number just takes too long.")
@pytest.mark.parametrize('Ms', np.arange(3, 5))
@pytest.mark.parametrize('Mt', np.arange(3, 5))
def test_winding_number_sphere(Ms, Mt):
    unitSphere = SplineSphere((Ms, Mt))
    unitSphere.initialize_sphere(1.0, np.array([0, 0, 0]))

    assert unitSphere.isInside(np.array([0.0, 0.0, 0.0])) == 1
    assert unitSphere.isInside(np.array([0.0, 0.5, 0.0])) == 1

    assert unitSphere.isInside(np.array([1.0, 0.0, 0.0])) == 0.5
    assert unitSphere.isInside(np.array([0.0, 1.0, 0.0])) == 0.5

    assert unitSphere.isInside(np.array([25.0, 25.0, 25.0])) == 0
    assert unitSphere.isInside(np.array([5.0, 5.0, 5.0])) == 0


@pytest.mark.parametrize('Ms', np.arange(3, 5))
@pytest.mark.parametrize('Mt', np.arange(3, 5))
@pytest.mark.parametrize('radius', np.logspace(-6, 6, 3))
@pytest.mark.parametrize('center', [(0, 0, 0), (-12.3, 37, 1337)])
def test_value_parametrisation(Ms, Mt, radius, center):
    unitSphere = SplineSphere((Ms, Mt))
    unitSphere.initialize_sphere(radius=radius, center=center)

    s_rate_t = 20
    s_rate_s = 20

    vals_comp = []
    vals_th = []

    for j in range(0, (s_rate_s * (Ms - 1)) + 1):
        for i in range(0, s_rate_t * Mt):
            s = j / s_rate_s
            t = i / s_rate_t
            phi = t * np.pi * 2.0 / Mt
            theta = s * np.pi / (Ms - 1)

            val = unitSphere.parametersToWorld((s, t))
            val_th = center + radius * np.array(
                (np.cos(phi) * np.sin(theta),
                 np.sin(phi) * np.sin(theta),
                 np.cos(theta)))
            vals_comp.append(val)
            vals_th.append(val_th)

    np.testing.assert_array_almost_equal(vals_comp, vals_th)


@pytest.mark.parametrize('Ms', np.arange(3, 5))
@pytest.mark.parametrize('Mt', np.arange(3, 5))
@pytest.mark.parametrize('radius', np.logspace(-6, 6, 3))
@pytest.mark.parametrize('center', [(0, 0, 0), (-12.3, 37, 1337)])
def test_derivatives(Ms, Mt, radius, center):
    s_rate_s = 20
    s_rate_t = 20

    unitSphere = SplineSphere((Ms, Mt))
    unitSphere.initialize_sphere(radius=radius, center=center)

    vals_comp_ds = []
    vals_th_ds = []

    vals_comp_dt = []
    vals_th_dt = []
    for j in range(0, (s_rate_s * (Ms - 1)) + 1):
        for i in range(0, s_rate_t * Mt):
            s = j / s_rate_s
            t = i / s_rate_t
            phi = t * np.pi * 2.0 / Mt
            theta = s * np.pi / (Ms - 1)

            dt_cal = unitSphere.parametersToWorld((s, t), ds=False, dt=True)

            dt_th = radius * np.array(
                (-np.sin(phi) * np.sin(theta),
                 np.cos(phi) * np.sin(theta),
                 0))

            ds_cal = unitSphere.parametersToWorld((s, t), ds=True, dt=False)
            ds_th = radius * np.array(
                (np.cos(phi) * np.cos(theta),
                 np.sin(phi) * np.cos(theta),
                 -np.sin(theta)))

            vals_comp_dt.append(dt_cal)
            vals_th_dt.append(dt_th)

            vals_comp_ds.append(ds_cal)
            vals_th_ds.append(ds_th)

    np.testing.assert_almost_equal(vals_comp_ds, vals_th_ds)
    np.testing.assert_almost_equal(vals_comp_dt, vals_th_dt)
