import numpy as np
from sft.spline.basis import H3


def test_hermite_basis():
    M = 8
    II = np.repeat(np.arange(M)[:, None], M, axis=1)
    JJ = np.repeat(np.arange(M)[None, :], M, axis=0)

    M = M
    I = II - JJ

    A = np.mod(I, M)
    Phi11 = np.zeros(I.shape)
    Phi11[A == 0] = 26.0 / (35 * M)
    Phi11[A == 1] = 9.0 / (70 * M)
    Phi11[A == M - 1] = 9.0 / (70 * M)
    Phi11ref = Phi11

    Phi11comp = np.array([
        [H3().h31PeriodicAutocorrelation(el, M) for el in row] for row in I])

    np.testing.assert_almost_equal(Phi11ref, Phi11comp)

    A = np.mod(I, M)
    Phi11 = np.zeros(I.shape)
    Phi11[A == 0] = 26.0 / (35 * M)
    Phi11[A == 1] = 9.0 / (70 * M)
    Phi11[A == M - 1] = 9.0 / (70 * M)
    Phi11 = M / (M - 1) * Phi11
    Phi11[0, 0] = .5 * Phi11[0, 0]
    Phi11[-1, -1] = .5 * Phi11[-1, -1]
    Phi11[-1, 0] = 0
    Phi11[0, -1] = 0
    Phi11ref = Phi11

    Phi11comp = np.array(
        [[H3().h31Autocorrelation(II[j, i], JJ[j, i], M)
          for i in range(0, len(II[j]))] for j in range(0, len(II))])

    np.testing.assert_almost_equal(Phi11ref, Phi11comp)

    A = np.mod(I, M)
    Phi22 = np.zeros(I.shape)
    Phi22[A == 0] = 2.0 / (105 * M)
    Phi22[A == 1] = -1.0 / (140 * M)
    Phi22[A == M - 1] = -1.0 / (140 * M)
    Phi22ref = Phi22

    Phi22comp = np.array([[H3().h32PeriodicAutocorrelation(el, M)
                           for el in row] for row in I])

    np.testing.assert_almost_equal(Phi22ref, Phi22comp)

    A = np.mod(I, M)
    Phi22 = np.zeros(I.shape)
    Phi22[A == 0] = 2.0 / (105 * M)
    Phi22[A == 1] = -1.0 / (140 * M)
    Phi22[A == M - 1] = -1.0 / (140 * M)
    Phi22 = M / (M - 1) * Phi22
    Phi22[0, 0] = .5 * Phi22[0, 0]
    Phi22[-1, -1] = .5 * Phi22[-1, -1]
    Phi22[-1, 0] = 0
    Phi22[0, -1] = 0
    Phi22ref = Phi22

    Phi22comp = np.array(
        [[H3().h32Autocorrelation(II[j, i], JJ[j, i], M)
          for i in range(0, len(II[j]))] for j in
         range(0, len(II))])

    np.testing.assert_almost_equal(Phi22ref, Phi22comp)

    A = np.mod(I, M)
    Phi12 = np.zeros(I.shape)
    Phi12[A == 1] = 13. / (420 * M)
    Phi12[A == M - 1] = -13. / (420 * M)
    Phi12ref = Phi12

    Phi12comp = np.array([[H3().h3PeriodicCrosscorrelation(el, M)
                           for el in row] for row in I])

    np.testing.assert_almost_equal(Phi12ref, Phi12comp)

    A = np.mod(I, M)
    Phi12 = np.zeros(I.shape)
    Phi12[A == 1] = 13. / (420 * M)
    Phi12[A == M - 1] = -13. / (420 * M)
    Phi12 = M / (M - 1) * Phi12
    Phi12[0, 0] = 11. / (210 * (M - 1))
    Phi12[-1, -1] = -11. / (210 * (M - 1))
    Phi12[0, -1] = 0
    Phi12[-1, 0] = 0
    Phi12ref = Phi12

    Phi12comp = np.array(
        [[H3().h3Crosscorrelation(II[j, i], JJ[j, i], M)
          for i in range(0, len(II[j]))] for j in
         range(0, len(II))])

    np.testing.assert_almost_equal(Phi12ref, Phi12comp)