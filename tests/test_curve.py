import glob

import matplotlib.pyplot as plt
import numpy as np
import pytest
import os.path as osp
from skimage import io

from sft.spline.basis import Keys, B1, B3, EM
from sft.spline.curve_models import SplineCurve
from tests import TEST_DATA


@pytest.mark.parametrize('M', np.arange(7, 20))
def test_winding_number_curve(M):

    unitCircle_coordinates = np.asarray([
        [np.cos(2 * np.pi * i / M),
         np.sin(2 * np.pi * i / M)] for i in range(0, M)]
    )
    unitCircle = SplineCurve(M, EM(M, 2.0 * np.pi / M), closed=True)

    unitCircle.setCoefsFromKnots(unitCircle_coordinates)

    assert unitCircle.isInside(np.array([0, 0])) == 1
    assert unitCircle.isInside(np.array([0, 0.5])) == 1

    assert unitCircle.isInside(np.array([25, 25])) == 0
    assert unitCircle.isInside(np.array([5, 5])) == 0


@pytest.mark.parametrize('img_file', glob.glob(osp.join(TEST_DATA, "*.tif")))
def test_binary_mask(img_file, plot=False):
    img = io.imread(img_file)

    M = 30
    splineCurve = SplineCurve(M, B3(), True)

    splineCurve.setCoefsFromBinaryMask(img)

    samplingRate = 1000
    splineContour = splineCurve.sample(samplingRate)

    if plot:
        fig, ax = plt.subplots()
        ax.imshow(img, cmap=plt.cm.gray)
        ax.scatter(splineContour[:, 1], splineContour[:, 0], s=1)
        plt.scatter(splineCurve.coefs[:,1],splineCurve.coefs[:,0])
        plt.show()

    t0 = 0
    tf = M

    samples, step = np.linspace(t0, tf, 1000, endpoint=False, retstep=True)
    length_direct = sum([((splineCurve.parameterToWorld(i, dt=True)[0] ** 2 +
                        splineCurve.parameterToWorld(i, dt=True)[1] ** 2) ** (
                1 / 2)) * step for i in samples])

    sft_arcLength = splineCurve.arcLength(t0, tf)

    np.testing.assert_almost_equal(sft_arcLength, length_direct, decimal=4)


@pytest.mark.parametrize('basis_func', [B1(),
                                        B3(),
                                        EM(M=4, alpha=2 * np.pi/4),
                                        Keys()])
def test_interpolate_4points(basis_func, plot=False):
    # Some pixel coordinates to interpolate
    coordinates = np.array([[0, 100], [100, 0], [0, -100], [-100, 0]])

    M = len(coordinates)

    # A Sampling rate to represent the continuous spline
    samplingRate = 100

    # Linear B-spline interpolation
    curve = SplineCurve(M, basis_func, closed=True)
    curve.setCoefsFromKnots(coordinates)
    discreteContour = curve.sample(samplingRate)

    if plot:
        plt.scatter(discreteContour[:, 0], discreteContour[:, 1])
        plt.show()


@pytest.mark.parametrize('basis_func', [B1(),
                                        B3(),
                                        EM(M=7, alpha=2 * np.pi/7),
                                        Keys()])
@pytest.mark.parametrize('closed', [True, False])
def test_interpolation_7points(basis_func, closed, plot=False):
    # Some points to interpolate
    points = np.array([
        [-2.0, 0.5],
        [0.0, 1.0],
        [1.0, 1.0],
        [2.0, 0.0],
        [1.0, -1.0],
        [0.0, -1.0],
        [-2.0, -0.5]
    ])

    samplingRate = 100

    M = len(points)

    splineCurve = SplineCurve(M, basis_func, closed)
    splineCurve.setCoefsFromKnots(points)

    # 1D
    if splineCurve.closed:
        # parameters = np.linspace(0, M, samplingRate * M, endpoint=False)
        parameters = np.array(range(0, (samplingRate * M))) / float(samplingRate)
    else:
        # parameters = np.linspace(0, M, samplingRate * (M - 1), endpoint=True)
        parameters = np.array(range(0, (samplingRate * (M - 1)) + 1)) / float(samplingRate)

    curves_coordinates = splineCurve.sample(samplingRate)

    if plot:
        plt.plot(parameters, curves_coordinates)

        # Add the control points
        plt.plot(range(M), points, 'bo')
        plt.show()

    # 2D
    samplingRate = 10
    contour = splineCurve.sample(samplingRate)

    if plot:
        plt.scatter(contour[:, 0], contour[:, 1], s=1, color="black")
        plt.scatter(points[:, 0], points[:, 1], color="r")
        plt.show()

    numberOfPoints = samplingRate * splineCurve.M
    contour = splineCurve.sampleArcLength(numberOfPoints)
    if plot:
        plt.scatter(contour[:, 0], contour[:, 1], s=1, color="black")
        plt.scatter(points[:, 0], points[:, 1], color="r")
        plt.show()
