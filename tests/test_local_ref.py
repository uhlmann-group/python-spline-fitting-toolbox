#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
from skimage import measure, io
from sft.spline.basis import EM, B3
from sft.spline.curve_models import SplineCurve

if __name__ == "__main__":

    basis = EM(3.0, 2.0 * np.pi / 3.0)
    support = basis.support

    basis2 = EM(3.0, np.pi / 3.0)
    mask = basis2.refinementMask()
    print(mask)

    # Centered - sanity check
    centeredVals = np.linspace(-support / 2.0 - 2, support / 2.0 + 2, 1000)
    centeredPoints = [basis.value(t - 1) for t in centeredVals]
    centeredRefinedPoints = [
        np.sum([mask[k] * basis2.value(2.0 * (t - 1) - (k - support / 2.0))
                for k in range(0, len(mask))])
        for t in centeredVals]

    plt.plot(centeredVals, centeredPoints, color='blue')
    plt.plot(centeredVals,centeredRefinedPoints,color='red')
    plt.show()

    # Centered - cute overlay
    centeredRefinedBasisPoints = np.asarray(
        [[mask[k] * basis2.value(2.0 * (t - 1) - (k - support / 2.0))
          for t in centeredVals]
         for k in range(0, len(mask))])

    plt.plot(centeredVals, centeredPoints, color='blue')
    for vals in centeredRefinedBasisPoints:
        plt.plot(centeredVals, vals, color='red')

    plt.show()

    # Causal - sanity check
    causalVals = np.linspace(-2, support + 2, 1000)
    causalPoints = [basis.value(t - support / 2.0) for t in causalVals]
    causalRefinedPoints = [np.sum([mask[k] * basis2.value(2.0 * t - support / 2.0 - k)
                                   for k in range(0, len(mask))])
                           for t in causalVals]

    plt.plot(causalVals, causalPoints, color='blue')
    plt.plot(causalVals, causalRefinedPoints, color='red')

    plt.show()

    # Causal - cute overlay
    causalRefinedBasisPoints = np.asarray(
        [[mask[k] * basis2.value(2.0 * t - support / 2.0 - k)
          for t in causalVals]
         for k in range(0, len(mask))])

    plt.plot(causalVals, causalPoints, color='blue')
    for vals in causalRefinedBasisPoints:
        plt.plot(causalVals, vals, color='red')

    plt.show()

    basis3 = EM(3.0, np.pi / 6.0)
    mask2 = basis3.refinementMask()
    print(mask)

    # Unit test
    centeredRefinedBasisPoints2 = np.asarray([[mask[3] * mask2[k] * basis3.value(
        2.0 * (2.0 * (t - 1) - (3 - support / 2.0)) - (k - support / 2.0)) for t in centeredVals] for k in
                                              range(0, len(mask2))])

    plt.plot(centeredVals, centeredPoints, color='blue')
    for vals in centeredRefinedBasisPoints:
        plt.plot(centeredVals, vals, color='red')
    for vals in centeredRefinedBasisPoints2:
        plt.plot(centeredVals, vals, color='green')

    plt.show()

    basis4 = EM(3.0, np.pi / 12.0)
    mask3 = basis4.refinementMask()
    print(mask)

    # Unit test
    centeredRefinedBasisPoints3 = np.asarray([[mask[3] * mask2[2] * mask3[k] * basis3.value(
        2.0 * (2.0 * (2.0 * (t - 1) - (3 - support / 2.0)) - (2 - support / 2.0)) - (k - support / 2.0)) for t in
                                               centeredVals] for k in range(0, len(mask2))])

    plt.plot(centeredVals, centeredPoints, color='blue')
    for vals in centeredRefinedBasisPoints:
        plt.plot(centeredVals, vals, color='red')
    for vals in centeredRefinedBasisPoints2:
        plt.plot(centeredVals, vals, color='green')
    for vals in centeredRefinedBasisPoints3:
        plt.plot(centeredVals, vals, color='purple')

    plt.show()

    # ### Some attempt to generalize to spline curves

    coefs = [0.2, 0.6, 0.4, 1.0]
    basis = B3()
    support = basis.support
    mask = basis.refinementMask()

    vals = np.linspace(-support / 2.0 - 2, len(coefs) + support / 2.0 + 2, 1000 * (len(coefs) - 1))
    sumFunction = [sum([coefs[k] * basis.value(t - k) for k in range(0, len(coefs))]) for t in vals]
    parts = [[coefs[k] * basis.value(t - k) for t in vals] for k in range(0, len(coefs))]

    plt.plot(vals, sumFunction, color='blue')
    for p in parts:
        plt.plot(vals, p, color='blue', linestyle='dashed')

    plt.plot(vals, sumFunction, color='blue')

    plt.show()

    coefs[1] = [mask[k] * coefs[1] for k in range(0, len(mask))]
    print(coefs)

    parts1 = [[coefs[k] * basis.value(t - k) for t in vals] for k in [0, 2, 3]]
    parts2 = [[coefs[1][k] * basis.value(2.0 * (t - 1) - (k - support / 2.0)) for t in vals] for k in
              range(0, len(coefs[1]))]

    plt.plot(vals, sumFunction, color='blue')
    for p in parts1:
        plt.plot(vals, p, color='blue', linestyle='dashed')
    for p in parts2:
        plt.plot(vals, p, color='red', linestyle='dashed')

    plt.show()

    coefs[1][2] = [mask[k] * coefs[1][2] for k in range(0, len(mask))]
    coefs[1][3] = [mask[k] * coefs[1][3] for k in range(0, len(mask))]
    print(coefs)

    parts1 = [[coefs[k] * basis.value(t - k) for t in vals] for k in [0, 2, 3]]
    parts2 = [[coefs[1][k] * basis.value(2.0 * (t - 1) - (k - support / 2.0)) for t in vals] for k in [0, 1, 4]]
    parts31 = [
        [coefs[1][2][k] * basis.value(2.0 * (2.0 * (t - 1) - (2 - support / 2.0)) - (k - support / 2.0)) for t in vals] for
        k in range(0, len(coefs[1][2]))]
    parts32 = [
        [coefs[1][3][k] * basis.value(2.0 * (2.0 * (t - 1) - (3 - support / 2.0)) - (k - support / 2.0)) for t in vals] for
        k in range(0, len(coefs[1][3]))]

    plt.plot(vals, sumFunction, color='blue')
    for p in parts1:
        plt.plot(vals, p, color='blue', linestyle='dashed')
    for p in parts2:
        plt.plot(vals, p, color='red', linestyle='dashed')
    for p in parts31:
        plt.plot(vals, p, color='green', linestyle='dashed')
    for p in parts32:
        plt.plot(vals, p, color='green', linestyle='dashed')

    plt.show()

    coefs[1][2][1] = [mask[k] * coefs[1][2][1] for k in range(0, len(mask))]
    print(coefs)

    parts1 = [[coefs[k] * basis.value(t - k) for t in vals] for k in [0, 2, 3]]
    parts2 = [[coefs[1][k] * basis.value(2.0 * (t - 1) - (k - support / 2.0)) for t in vals] for k in [0, 1, 4]]
    parts31 = [
        [coefs[1][2][k] * basis.value(2.0 * (2.0 * (t - 1) - (2 - support / 2.0)) - (k - support / 2.0)) for t in vals] for
        k in [0, 2, 3, 4]]
    parts32 = [
        [coefs[1][3][k] * basis.value(2.0 * (2.0 * (t - 1) - (3 - support / 2.0)) - (k - support / 2.0)) for t in vals] for
        k in range(0, len(coefs[1][3]))]
    parts4 = [[coefs[1][2][1][k] * basis.value(
        2.0 * (2.0 * (2.0 * (t - 1) - (2 - support / 2.0)) - (1 - support / 2.0)) - (k - support / 2.0)) for t in vals] for
              k in range(0, len(coefs[1][2][1]))]

    plt.plot(vals, sumFunction, color='blue')
    for p in parts1:
        plt.plot(vals, p, color='blue', linestyle='dashed')
    for p in parts2:
        plt.plot(vals, p, color='red', linestyle='dashed')
    for p in parts31:
        plt.plot(vals, p, color='green', linestyle='dashed')
    for p in parts32:
        plt.plot(vals, p, color='green', linestyle='dashed')
    for p in parts4:
        plt.plot(vals, p, color='purple', linestyle='dashed')

    plt.show()
    print(coefs)

    def localSum(t, m, c):
        val = 0.0
        for l in range(0, len(c)):
            if isinstance(c[l], list):
                val += localSum(2.0 * t - (l - support / 2.0), m + 1, c[l])
            else:
                if isinstance(basis, EM):
                    newBasis = EM(basis.M, basis.alpha / (2 ** m))
                else:
                    newBasis = basis
                val += c[l] * newBasis.value(2.0 * t - (l - support / 2.0))
        return val


    def getSampled(basis, coefs, vals):
        points = np.zeros((len(vals)))
        for i in range(0, len(vals)):
            for k in range(0, len(coefs)):
                if vals[i] - k > -support / 2.0 and vals[i] - k < support / 2.0:
                    if isinstance(coefs[k], (list, np.ndarray)):
                        points[i] += localSum(vals[i] - k, 1, coefs[k])
                    else:
                        points[i] += coefs[k] * basis.value(vals[i] - k)
        return points


    vals = np.linspace(-support / 2.0 - 2, len(coefs) + support / 2.0 + 2, 1000 * (len(coefs) - 1))
    points = getSampled(basis, coefs, vals)
    plt.plot(vals, sumFunction, color='blue')
    plt.plot(vals, points, color='green')

    plt.show()

    import copy

    coefs2 = copy.deepcopy(coefs)

    coefs2[1][2][1][2] *= 4.0

    points = getSampled(basis, coefs2, vals)
    # plt.plot(vals,sumFunction,color='blue')
    plt.plot(vals, points, color='blue')

    parts1 = [[coefs2[k] * basis.value(t - k) for t in vals] for k in [0, 2, 3]]
    parts2 = [[coefs2[1][k] * basis.value(2.0 * (t - 1) - (k - support / 2.0)) for t in vals] for k in [0, 1, 4]]
    parts31 = [
        [coefs2[1][2][k] * basis.value(2.0 * (2.0 * (t - 1) - (2 - support / 2.0)) - (k - support / 2.0)) for t in vals] for
        k in [0, 2, 3, 4]]
    parts32 = [
        [coefs2[1][3][k] * basis.value(2.0 * (2.0 * (t - 1) - (3 - support / 2.0)) - (k - support / 2.0)) for t in vals] for
        k in range(0, len(coefs2[1][3]))]
    parts4 = [[coefs2[1][2][1][k] * basis.value(
        2.0 * (2.0 * (2.0 * (t - 1) - (2 - support / 2.0)) - (1 - support / 2.0)) - (k - support / 2.0)) for t in vals] for
              k in range(0, len(coefs2[1][2][1]))]

    plt.plot(vals, sumFunction, color='blue')
    for p in parts1:
        plt.plot(vals, p, color='blue', linestyle='dashed')
    for p in parts2:
        plt.plot(vals, p, color='red', linestyle='dashed')
    for p in parts31:
        plt.plot(vals, p, color='green', linestyle='dashed')
    for p in parts32:
        plt.plot(vals, p, color='green', linestyle='dashed')
    for p in parts4:
        plt.plot(vals, p, color='purple', linestyle='dashed')
    plt.show()

    # ### Some attempt to use that in the context of spline fitting

    img = io.imread('data/catMask.tif')

    contours = measure.find_contours(img, 0)
    contours = contours[0]

    randomInput = contours[30:161, 1]
    plt.plot(range(0, len(randomInput)), randomInput, color='blue')
    plt.show()

    print(len(randomInput))

    M = 6
    basis = B3()
    curve = SplineCurve(M, basis, closed=False)
    support = basis.support

    # Let's start with M uniformly distributed control points
    samplingRate = int((len(randomInput) - 1) / (M - 1))
    knots = [randomInput[samplingRate * i] for i in range(0, M)]
    print(knots)
    curve.getCoefsFromKnots(knots)

    points = curve.sample(samplingRate)
    plt.plot(range(0, len(randomInput)), randomInput, color='blue')
    plt.plot(range(0, len(randomInput)), points, color='green')
    plt.show()

    def rmse(trueVals, estimatedVals):
        if len(trueVals) is not len(estimatedVals):
            raise ValueError(f"trueVals is of length {len(trueVals)} and "
                             f"estimatedVals is of length {len(estimatedVals)}")

        return np.linalg.norm(trueVals - estimatedVals)

    # Compute RMSE (= abs difference between actual
    # data points and interpolated points) in each interval.
    errors = {}
    for k in range(0, M):
        lowerBound = np.max((0, (k - (support / 2.0))))
        upperBound = np.min((M - 1, (k + (support / 2.0))))
        vals = np.linspace(lowerBound, upperBound,
                           int(samplingRate * (upperBound - lowerBound)))
        interval = getSampled(basis, curve.coefs, vals)

        errors[k] = rmse(randomInput[int(lowerBound * samplingRate):int(upperBound * samplingRate)], interval)
        # print(error)

    # The one with the poorest RMSE is allowed to subdivide first
    subdivided = np.array([])

    # while errors:
    maxIndex = 3
    # max(errors, key=errors.get)

    if errors[maxIndex] > 1e-6:
        mask = basis.refinementMask()
        localKnots = [randomInput[np.min(
            (np.max((int(((2.0 * maxIndex - (l - support / 2.0)) / 2.0) * samplingRate), 0)), len(randomInput) - 1))] for
                      l in range(0, len(mask))]
        localCoefs = basis.filterSymmetric(localKnots)

        # Iterate (=subdivide) the interval if it improves the RMSE over a given threshold
        # Open TODO: adapting spline filters for refined basis...

        coefs = np.ndarray.tolist(curve.coefs)
        # Sanity check: we get back the original basis
        # coefs[maxIndex]=[mask[l]*coefs[maxIndex] for l in range(0,len(mask))]
        # This clearly doesn't work sadly
        coefs[maxIndex] = [mask[k] * localCoefs[k] for k in range(0, len(mask))]
        curve.coefs = np.asarray(coefs)
    del errors[maxIndex]

    # Do one round at the time, i.e. intervals are not allowed to get
    # refined twice before everyone else is reviewed
    # Continue until all intervals have a decent (= under threshold) RMSE

    vals = np.linspace(0, M - 1, (samplingRate * (M - 1)) + 1)
    points = getSampled(basis, curve.coefs, vals)
    plt.plot(range(0, len(randomInput)), randomInput, color='blue')
    plt.plot(range(0, len(randomInput)), points, color='green')
    plt.show()