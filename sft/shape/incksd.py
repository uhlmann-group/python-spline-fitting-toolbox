import numpy as np
import random
import time
import matplotlib.pyplot as plt
import shape_space_utils as ssu
import torch
import scipy.optimize


class KSD:
    def __init__(self, numAnchors, N, configType, sparsity, numAtoms):
        self.configType = configType
        self.numAnchors = numAnchors
        self.N = N

        self.Phi = ssu.getPhi(self.configType, self.numAnchors)

        self.sparsity = sparsity
        self.numAtoms = numAtoms

        self.D_c = None
        self.A_c = None

    def initialize(self, dataset, K):
        """Initialize the dictionary: randomly copy the existing data."""
        D_c = np.zeros((self.N, self.numAtoms), dtype=complex)

        indices = np.arange(K)
        random.shuffle(indices)
        for j in range(self.numAtoms):
            k = indices[j]
            D_c[:, j] = dataset[k]

        return D_c

    def ORMPCholesky(self, D_c, dataset):  # columns of D_c must be preshapes
        """Order Recursive Matching Pursuit with Cholesky-based optimisation,
        as in the SPAMS toolbox of Mairal et al. (2009).
        This is a direct adaptation of their code in C++ to the complex setting
        with a Hermitian product Phi.
        The columns of D_c must all be preshapes.
        """

        J = D_c.shape[1]
        K = len(dataset)
        A_c = np.zeros((J, K), dtype=complex)
        G = D_c.T.conj() @ self.Phi @ D_c

        for k in range(K):
            z = dataset[k]
            vM = np.zeros((self.sparsity), dtype=complex)
            rM = np.zeros((self.sparsity), dtype=int)

            Un = np.zeros((self.sparsity, self.sparsity), dtype=complex)
            Undn = np.zeros((J, self.sparsity), dtype=complex)
            Gs = np.zeros((self.sparsity, J), dtype=complex)
            norm2 = np.ones(J)

            rM[:] = -1
            Rdn = D_c.T.conj() @ self.Phi @ z
            scores = Rdn.copy()

            for l in range(self.sparsity):
                currind = np.argmax(np.abs(scores))
                if np.abs(scores[currind]) < 1e-8:
                    break

                RU = scores[currind]
                vM[l] = RU
                rM[l] = currind

                Un[l, l] = -1
                Un[:l, l] = Un[:l, :l] @ Undn[currind, :l].T
                Un[:, l] = -Un[:, l] / np.sqrt(norm2[currind])
                if l == self.sparsity - 1:
                    break
                Gs[l] = G[currind]
                Undn[:, l] = Un[: l + 1, l].T.conj() @ Gs[: l + 1, :]

                Rdn = Rdn - vM[l] * Undn[:, l].conj()
                norm2 = norm2 - np.abs(Undn[:, l]) ** 2
                norm2[
                    norm2 < 0
                ] = 0  # sometimes happens to have small negative numbers
                non_null = norm2 > 1e-8
                scores = np.zeros(J, dtype=complex)
                scores[non_null] = Rdn[non_null] / np.sqrt(norm2)[non_null]

            vM = Un @ vM

            indices = [j for j in rM if j >= 0]
            A_c[indices, k] = vM[: len(indices)]

        return A_c

    def normalize(self, D_c):
        """Normalizing the columns of D_c, which are centered configurations"""
        J = D_c.shape[1]
        uu = ssu.normalizedUnitConfiguration(
            self.Phi, self.numAnchors, self.configType
        )

        for j in range(J):
            d = D_c[:, j]
            orth_d = ssu.hermitianProduct(uu, d, self.Phi) * uu
            d = d - orth_d
            no = ssu.configurationNorm(d, self.Phi)
            if no > 1e-8:
                D_c[:, j] = d / no
        return D_c

    def dicoUpdateBFGS(self, A, D0, shapes_r, incoherence=False, alpha=1.0):
        def F(Dv):
            Dx = Dv[: self.N * self.numAtoms].reshape(self.N, self.numAtoms)
            Dy = Dv[self.N * self.numAtoms :].reshape(self.N, self.numAtoms)

            Ax, Ay = split_xy(A, self.numAtoms)

            DA_re = Dx @ Ax - Dy @ Ay
            DA_im = Dx @ Ay + Dy @ Ax

            shapes_re, shapes_im = split_xy(shapes_r.T, self.N)

            diff_re = shapes_re - DA_re
            diff_im = shapes_im - DA_im
            E1 = np.sum(
                np.diag(
                    diff_re.T @ self.Phi @ diff_re
                    + diff_im.T @ self.Phi @ diff_im
                )
            )

            if incoherence:
                PD_re = self.Phi @ Dx
                PD_im = self.Phi @ Dy

                DPD_re = Dx.T @ PD_re - Dy.T @ PD_im
                DPD_im = Dx.T @ PD_im + Dy.T @ PD_re

                prod_re = DPD_re.T @ DPD_re - DPD_im.T @ DPD_im
                prod_im = DPD_re.T @ DPD_im + DPD_im.T @ DPD_re

                E2 = np.trace(prod_re + prod_im)
                return E1 + alpha * E2
            else:
                return E1

        def dDF(Dv):
            Dx = Dv[: self.N * self.numAtoms].reshape(self.N, self.numAtoms)
            Dy = Dv[self.N * self.numAtoms :].reshape(self.N, self.numAtoms)
            Ax, Ay = split_xy(A, self.numAtoms)

            DA_re = Dx @ Ax - Dy @ Ay
            DA_im = Dx @ Ay + Dy @ Ax

            shapes_re, shapes_im = split_xy(shapes_r.T, self.N)

            ZA_re = shapes_re @ Ax.T - shapes_im @ Ay.T
            ZA_im = shapes_re @ Ay.T + shapes_im @ Ax.T

            DAA_re = DA_re @ Ax.T - DA_im @ Ay.T
            DAA_im = DA_re @ Ay.T + DA_im @ Ax.T

            sum_re = 2 * self.Phi @ (-ZA_re + DAA_re)
            sum_im = 2 * self.Phi @ (-ZA_im + DAA_im)
            G1 = np.hstack([sum_re.flatten(), sum_im.flatten()])

            if incoherence:
                PD_re = self.Phi @ Dx
                PD_im = self.Phi @ Dy

                DPD_re = Dx.T @ PD_re - Dy.T @ PD_im
                DPD_im = Dx.T @ PD_im + Dy.T @ PD_re

                DDPD_re = Dx @ DPD_re - Dy @ DPD_im
                DDPD_im = Dx @ DPD_im + Dy @ DPD_re

                prod_re = 4 * self.Phi @ DDPD_re
                prod_im = 4 * self.Phi @ DDPD_im

                G2 = np.hstack([prod_re.flatten(), prod_im.flatten()])
                return G1 + alpha * G2
            else:
                return G1

        D0v = np.hstack([D0[: self.N].flatten(), D0[self.N :].flatten()])

        res = scipy.optimize.minimize(F, D0v, method="L-BFGS-B", jac=dDF)

        Dv = res.x
        D = np.zeros(D0.shape)
        D[: self.N] = Dv[: self.N * self.numAtoms].reshape(
            self.N, self.numAtoms
        )
        D[self.N :] = Dv[self.N * self.numAtoms :].reshape(
            self.N, self.numAtoms
        )
        return D

    def torchLoss(
        self,
        Atorch,
        Dtorch,
        Phitorch,
        shapestorch,
        incoherence=False,
        alpha=1.0,
    ):
        Dx, Dy = split_xy(Dtorch, self.N)
        Ax, Ay = split_xy(Atorch, self.numAtoms)

        DA_re = Dx @ Ax - Dy @ Ay
        DA_im = Dx @ Ay + Dy @ Ax

        shapes_re, shapes_im = split_xy(shapestorch.t(), self.N)

        diff_re = shapes_re - DA_re
        diff_im = shapes_im - DA_im
        E1 = torch.diag(
            diff_re.t() @ Phitorch @ diff_re + diff_im.t() @ Phitorch @ diff_im
        ).sum()

        if incoherence:
            PD_re = Phitorch @ Dx
            PD_im = Phitorch @ Dy

            DPD_re = Dx.t() @ PD_re - Dy.t() @ PD_im
            DPD_im = Dx.t() @ PD_im + Dy.t() @ PD_re

            prod_re = DPD_re.t() @ DPD_re - DPD_im.t() @ DPD_im
            prod_im = DPD_re.t() @ DPD_im + DPD_im.t() @ DPD_re

            E2 = torch.trace(prod_re + prod_im)
            return E1 + alpha * E2
        else:
            return E1

    def learn(
        self,
        dataset,
        init=None,
        numIter=1000,
        kappa=1e-5,
        useTorch=False,
        incoherence=False,
        alpha=1.0,
        verbose=False,
    ):
        """2D Kendall Shape Dictionary classically alternates between:

        - a sparse coding step : the weights A are updated using a
        Cholesky-based Order Recursive Matching Pursuit (ORMP), as a direct
        adaptation to the complex setting of Mairal's implementation for the
        real setting in the  SPAMS toolbox.
        - a dictionary update : following the Method of Optimal Directions
        (MOD), we update D as

                D <- [z_1,...,z_K] @ A^H @ (A @ A^H)^{-1}
                D <- Pi_S(D) (center and normalize all the non-null atoms d_j)

        and then replace under-utilized or null atoms by randomly picked data.
        An atom d_j is arbitrarily said to be under-utilized if
                (nb of data using d_j) / (K*N0) < 1 / (50*numAtoms)

        Parameters:
            - dataset in C^{(K,n)} is a complex array containing the
            horizontally stacked dataset [z_1,...,z_K]^T
            - sparsity determines the L0 sparsity, N0, of the weights a_k
            - numAtoms fixes the number of atoms that we want to learn
            - init = None initializes the dictionary with randomly picked
            data shapes.
                if init is a given (n,numAtoms) complex array, then the
                initialization starts with init.
            - numIter is the number of iterations
            - if verbose == True, the algorithm keeps track of the loss
            function E to be minimized at each iteration.

        """
        K = len(dataset)

        shape_r = np.zeros((K, 2 * self.N))
        shape_r[:, : self.N] = dataset.real
        shape_r[:, self.N :] = dataset.imag

        if verbose:
            print("Initializing the dictionary.")
        if type(init) == np.ndarray:
            D_c = init
        else:
            D_c = self.initialize(dataset, K)

        if useTorch:
            shapestorch = torch.autograd.Variable(
                torch.from_numpy(shape_r).type(torch.FloatTensor)
            )
            Phitorch = torch.autograd.Variable(
                torch.from_numpy(self.Phi).type(torch.FloatTensor)
            )

            D = np.zeros((2 * self.N, self.numAtoms))
            D[: self.N] = D_c.real
            D[self.N :] = D_c.imag
            Dtorch = torch.autograd.Variable(
                torch.from_numpy(D).type(torch.FloatTensor), requires_grad=True
            )
            optimizer = torch.optim.SGD([Dtorch], lr=kappa)

        if verbose:
            lossCurve = np.array([])
            print("Let's wait for {} iterations...".format(numIter))
            start = time.time()

        for t in range(numIter):
            if verbose:
                if numIter <= 100:
                    mod = 10
                else:
                    mod = 100
                if t % mod == 0:
                    print("t =", t)

            # Sparse coding
            A_c = self.ORMPCholesky(D_c, dataset)

            if verbose:
                diffs = dataset.T - D_c @ A_c
                E1 = np.diag(diffs.T.conj() @ self.Phi @ diffs).sum().real
                if incoherence:
                    DPD = D_c.T.conj() @ self.Phi @ D_c
                    E2 = np.trace(DPD.T.conj() @ DPD).real
                    E = E1 + alpha * E2
                else:
                    E = E1
                lossCurve = np.append(lossCurve, E)

            # Dictionary update
            A = np.zeros((2 * self.numAtoms, K))
            A[: self.numAtoms] = A_c.real
            A[self.numAtoms :] = A_c.imag

            if useTorch:

                def closure():
                    optimizer.zero_grad()
                    Atorch = torch.autograd.Variable(
                        torch.from_numpy(A).type(torch.FloatTensor)
                    )
                    loss = self.torchLoss(
                        Atorch,
                        Dtorch,
                        Phitorch,
                        shapestorch,
                        incoherence,
                        alpha,
                    )
                    loss.backward()
                    return loss

                optimizer.step(closure)

                D = Dtorch.data.numpy()

            else:
                D = np.zeros((2 * self.N, self.numAtoms))
                D[: self.N] = D_c.real
                D[self.N :] = D_c.imag

                D = self.dicoUpdateBFGS(A, D, shape_r, incoherence, alpha)

            D_c.real = D[: self.N]
            D_c.imag = D[self.N :]
            D_c = self.normalize(D_c)

            purge_j = np.where(
                (np.abs(A_c) > 1e-3).sum(axis=1) / K
                < self.sparsity / (5 * self.numAtoms)
            )[0]
            for j in range(self.numAtoms):
                if (
                    ssu.configurationNorm(D_c[:, j], self.Phi) < 1e-8
                    or j in purge_j
                ):
                    if verbose:
                        print("purged ", j, "at iteration", t)
                    D_c[:, j] = dataset[np.random.randint(K)]

            if useTorch:
                D = np.zeros((2 * self.N, self.numAtoms))
                D[: self.N] = D_c.real
                D[self.N :] = D_c.imag
                Dtorch.data = torch.from_numpy(D).type(torch.FloatTensor)

        if verbose:
            print("computing the final weights...")
        A_c = self.ORMPCholesky(D_c, dataset)

        if verbose:
            elapsed = time.time() - start
            print(
                "duration of the algorithm: ", np.round(elapsed, 2), "seconds"
            )

        diffs = dataset.T - D_c @ A_c
        E1 = np.diag(diffs.T.conj() @ self.Phi @ diffs).sum().real
        if incoherence:
            DPD = D_c.T.conj() @ self.Phi @ D_c
            E2 = np.trace(DPD.T.conj() @ DPD).real
            E = E1 + alpha * E2
        else:
            E = E1

        if verbose:
            lossCurve = np.append(lossCurve, E)
            plt.figure()
            plt.plot(np.arange(len(lossCurve)), lossCurve)
            plt.title("Loss curve")
            plt.show()

        self.A_c = A_c
        self.D_c = D_c
        return E1


def greedyOrder(inputA):
    """Gives a permutation of the dictionary atoms, so that the first atom of
    the dictionary is the one for which the modulus of A[:,0] is maximal;
    then the second atom for which..."""
    import copy

    A = copy.deepcopy(inputA)
    if not np.iscomplexobj(A):
        J = A.shape[0] // 2
        A = ssu.realToComplex(A.T).T
    J = A.shape[0]
    K = A.shape[1]
    sort = np.array([], dtype=int)
    k = 0
    if K == 1:
        j0 = np.argmax(np.abs(A[:, k]))
        sort = np.append(sort, j0)
        for j in range(J):
            if j not in sort:
                sort = np.append(sort, j)
        return sort
    # here, K >= 2
    while len(sort) < J and k < K - 1:
        while (
            k < K - 1 and np.argmax(np.abs(A[:, k])) in sort
        ):  # k < K-1 sometimes for K big too long
            k = k + 1
        A[sort, k] = 0
        j0 = np.argmax(np.abs(A[:, k]))
        if j0 not in sort:
            sort = np.append(sort, j0)
    for j in range(J):
        if j not in sort:
            sort = np.append(sort, j)
    return sort, A


def split_xy(r, L):
    # r real or torch, 2*L or 2*L \times ...
    rx = r[:L]
    ry = r[L:]
    return rx, ry
