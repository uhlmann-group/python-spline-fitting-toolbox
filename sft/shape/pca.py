import numpy as np
import shape_space_utils as ssu
import scipy


def shapePCA(data, numAnchors, N, configType, c=3.0):
    K = len(data)

    Phi = ssu.getPhi(configType, numAnchors)
    frechetMean = ssu.meanFrechet(data, Phi, numAnchors, configType)

    aligned = np.zeros(data.shape, dtype=complex)
    for k in range(0, K):
        aligned[k] = ssu.log(frechetMean, data[k], Phi)

    pca = PCA(numAnchors, N, configType, frechetMean)
    pca.fit(aligned)

    pcWeights = np.zeros((K, 2 * pca.N))
    for k in range(0, K):
        pcWeights[k] = pca.project(aligned[k])

    modes = []
    count = len(np.where(pca.diag > 1e-6)[0])
    for i in range(count):
        mode = (
            c
            * np.std(pcWeights[:, i])
            * np.sqrt(pca.diag[i])
            * pca.complexPC[:, i]
        )
        modes.append(
            [
                ssu.exponentialMap(frechetMean, mode, Phi),
                ssu.exponentialMap(frechetMean, -mode, Phi),
            ]
        )

    modes = np.asarray(modes)
    return pca, modes


def shapePCReconstruction(data, pca, count=0):
    K = len(data)
    Phi = ssu.getPhi(pca.configType, pca.numAnchors)

    error = []
    reconstructedShapes = np.zeros(data.shape, dtype=complex)
    for k in range(0, K):
        aligned = ssu.log(pca.frechetMean, data[k], Phi)
        reconstruction = pca.reconstruct(aligned, count)
        reconstructedShapes[k] = ssu.exponentialMap(
            pca.frechetMean, reconstruction, Phi
        )
        originalShape = ssu.exponentialMap(pca.frechetMean, aligned, Phi)
        error.append(
            np.round(
                ssu.configurationNorm(
                    originalShape - reconstructedShapes[k], Phi
                ).real,
                4,
            )
        )

    return reconstructedShapes, error


def shapePCWeights(data, pca):
    K = len(data)
    Phi = ssu.getPhi(pca.configType, pca.numAnchors)

    pcWeights = np.zeros((K, 2 * pca.N))
    for k in range(0, K):
        aligned = ssu.log(pca.frechetMean, data[k], Phi)
        pcWeights[k] = pca.project(aligned)

    return pcWeights


class PCA:
    def __init__(self, numAnchors, N, configType, frechetMean):
        """- N number of parameters
        - configType type of configuration
        """
        self.configType = configType
        self.numAnchors = numAnchors
        self.N = N
        self.frechetMean = frechetMean

        self.getPsi()

        self.pcPhi = None
        self.diag = None
        self.complexPc = None

    def getPsi(self):
        if self.configType is ssu.configurationType.LANDMARKS:
            self.Psi = np.eye(2 * self.numAnchors)

        elif (
            self.configType is ssu.configurationType.CLOSEDBSPLINE
            or ssu.configurationType.CLOSEDHSPLINE
            or ssu.configurationType.OPENHSPLINE
        ):
            Phi = ssu.getPhi(self.configType, self.numAnchors)
            self.Psi = np.concatenate(
                (np.hstack((Phi, 0 * Phi)), np.hstack((0 * Phi, Phi)))
            )

        else:
            raise NotImplementedError(
                "Unknown configuration type (" + str(self.configType) + ")."
            )

    def fit(self, data):
        """Classical PCA

        Parameters:
            - data in C^{(K,n)} is a complex array containing the horizontally stacked dataset [z_1,...,z_K]^T


        Returns:
            - Eigenvalues
            - Eigenvectors (eigenmodes)

        """

        K = len(data)

        # mean = np.mean(data,0) # not necessarily zero
        # V = (data - mean)
        V = data

        sqrtPsi = scipy.linalg.sqrtm(self.Psi)

        Vr = np.zeros((K, 2 * self.N))
        Vr[:, : self.N] = V.real
        Vr[:, self.N :] = V.imag

        Y = Vr @ sqrtPsi
        Diag, Vmodes = np.linalg.eig(Y.T @ Y)
        Diag = Diag.real
        Vmodes = Vmodes.real

        sortindr = np.argsort(Diag)
        sortindr = sortindr[::-1]  # decreasing order of eigenvalues

        self.diag = Diag[sortindr]
        Vmodes = Vmodes[:, sortindr]

        Wmodes = (
            np.linalg.inv(sqrtPsi) @ Vmodes
        )  # eigenmodes in the tangent plane at m
        W = np.zeros((self.N, 2 * self.N), dtype=complex)
        W.real = Wmodes[: self.N, :]
        W.imag = Wmodes[self.N :, :]
        self.pcPhi = Vmodes
        self.complexPC = W

    def project(self, v):
        sqrtPsi = scipy.linalg.sqrtm(self.Psi)

        vr = np.zeros((2 * self.N))
        vr[: self.N] = v.real
        vr[self.N :] = v.imag

        pcWeight = np.zeros((2 * self.N))
        for k in range(2 * self.N):
            y = vr @ sqrtPsi
            pcWeight[k] = y @ self.pcPhi[:, k]

        return pcWeight

    def reconstruct(self, v, count=0):
        pcWeight = self.project(v)
        if count < 1:
            count = len(pcWeight)

        reconstruction = np.zeros((self.N), dtype=complex)
        for k in range(count):
            reconstruction += pcWeight[k] * self.complexPC[:, k]

        return reconstruction
