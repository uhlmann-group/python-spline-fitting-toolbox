import numpy as np
import random
import time
import matplotlib.pyplot as plt
import shape_space_utils as ssu


class KSD:
    def __init__(self, numAnchors, N, configType, sparsity, numAtoms):
        self.configType = configType
        self.numAnchors = numAnchors
        self.N = N

        self.Phi = ssu.getPhi(self.configType, self.numAnchors)

        self.sparsity = sparsity
        self.numAtoms = numAtoms

        self.D_c = None
        self.A_c = None

    def initialize(self, dataset, K):
        """Initialize the dictionary: randomly copy the existing data."""
        D_c = np.zeros((self.N, self.numAtoms), dtype=complex)

        indices = np.arange(K)
        random.shuffle(indices)
        for j in range(self.numAtoms):
            k = indices[j]
            D_c[:, j] = dataset[k]

        return D_c

    def ORMPCholesky(self, D_c, dataset):
        """Order Recursive Matching Pursuit with Cholesky-based optimisation,
        as in the SPAMS toolbox of Mairal et al. (2009).
        This is a direct adaptation of their code in C++ to the complex setting
        with a Hermitian product Phi.
        The columns of D_c must all be preshapes.
        """

        J = D_c.shape[1]
        K = len(dataset)
        A_c = np.zeros((J, K), dtype=complex)
        G = D_c.T.conj() @ self.Phi @ D_c

        for k in range(K):
            z = dataset[k]
            vM = np.zeros((self.sparsity), dtype=complex)
            rM = np.zeros((self.sparsity), dtype=int)

            Un = np.zeros((self.sparsity, self.sparsity), dtype=complex)
            Undn = np.zeros((J, self.sparsity), dtype=complex)
            Gs = np.zeros((self.sparsity, J), dtype=complex)
            norm2 = np.ones(J)

            rM[:] = -1
            Rdn = D_c.T.conj() @ self.Phi @ z
            scores = Rdn.copy()

            for l in range(self.sparsity):
                currind = np.argmax(np.abs(scores))
                if np.abs(scores[currind]) < 1e-8:
                    break

                RU = scores[currind]
                vM[l] = RU
                rM[l] = currind

                Un[l, l] = -1
                Un[:l, l] = Un[:l, :l] @ Undn[currind, :l].T
                Un[:, l] = -Un[:, l] / np.sqrt(norm2[currind])
                if l == self.sparsity - 1:
                    break
                Gs[l] = G[currind]
                Undn[:, l] = Un[: l + 1, l].T.conj() @ Gs[: l + 1, :]

                Rdn = Rdn - vM[l] * Undn[:, l].conj()
                norm2 = norm2 - np.abs(Undn[:, l]) ** 2
                norm2[
                    norm2 < 0
                ] = 0  # sometimes happens to have small negative numbers
                non_null = norm2 > 1e-8
                scores = np.zeros(J, dtype=complex)
                scores[non_null] = Rdn[non_null] / np.sqrt(norm2)[non_null]

            vM = Un @ vM

            indices = [j for j in rM if j >= 0]
            A_c[indices, k] = vM[: len(indices)]

        return A_c

    def reciprocal(self, sigmas):
        """Auxiliary function of KSD_optimal_directions().
        Given a 1D array of non-negative elements called sigmas, with possibly
        zero elements,
        returns the array of multiplicative inverses whenever possible, and
        leaves the zeroes."""
        sigmas_rec = np.zeros_like(sigmas)
        for i, x in enumerate(sigmas):
            if x != 0:
                sigmas_rec[i] = 1 / x
        return sigmas_rec

    def fillDiagonal(self, sigmas_rec, K):
        """Auxiliary function of KSD_optimal_directions().
        Fills in the diagonal of a matrix of shape (K,J)."""
        Sigma_rec = np.zeros((K, self.numAtoms))
        np.fill_diagonal(Sigma_rec, sigmas_rec)
        return Sigma_rec

    def normalize(self, D_c):
        """Normalizing the columns of D_c, which are centered configurations"""
        J = D_c.shape[1]

        for j in range(J):
            d = D_c[:, j]
            no = ssu.configurationNorm(d, self.Phi)
            if no > 1e-8:
                D_c[:, j] = d / no
        return D_c

    def learn(self, dataset, init=None, numIter=100, verbose=False):
        """2D Kendall Shape Dictionary classically alternates between:

        - a sparse coding step : the weights A are updated using a
        Cholesky-based Order Recursive Matching Pursuit (ORMP), as a direct
        adaptation to the complex setting of Mairal's implementation for the
        real setting in the  SPAMS toolbox.
        - a dictionary update : following the Method of Optimal Directions
        (MOD), we update D as

                D <- [z_1,...,z_K] @ A^H @ (A @ A^H)^{-1}
                D <- Pi_S(D) (center and normalize all the non-null atoms d_j)

        and then replace under-utilized or null atoms by randomly picked data.
        An atom d_j is arbitrarily said to be under-utilized if
                (nb of data using d_j) / (K*N0) < 1 / (50*numAtoms)

        Parameters:
            - dataset in C^{(K,n)} is a complex array containing the
            horizontally stacked dataset [z_1,...,z_K]^T
            - sparsity determines the L0 sparsity, N0, of the weights a_k
            - numAtoms fixes the number of atoms that we want to learn
            - init = None initializes the dictionary with randomly picked
            data shapes.
                if init is a given (n,numAtoms) complex array, then the
                initialization starts with init.
            - numIter is the number of iterations
            - if verbose == True, the algorithm keeps track of the loss
            function E to be minimized at each iteration.

        """
        K = len(dataset)
        if type(init) == np.ndarray:
            D_c = init
        else:
            D_c = self.initialize(dataset, K)
        if verbose:
            if verbose:
                print("Initializing the dictionary.")
            lossCurve = np.array([])

        if verbose:
            print("Let's wait for {} iterations...".format(numIter))
            start = time.time()

        for t in range(numIter):
            if verbose:
                if numIter <= 100:
                    mod = 10
                else:
                    mod = 100
                if t % mod == 0:
                    print("t =", t)

            A_c = self.ORMPCholesky(D_c, dataset)

            if verbose:
                diffs = dataset.T - D_c @ A_c
                E = np.diag(diffs.T.conj() @ self.Phi @ diffs).sum().real
                lossCurve = np.append(lossCurve, E)

            try:
                Mat = np.linalg.inv(A_c @ A_c.T.conj())
            except np.linalg.LinAlgError:
                if verbose:
                    print("A @ A^H not invertible, using SVD")
                U, sigmas, VH = np.linalg.svd(A_c)
                sigmas_rec = self.reciprocal(sigmas)
                Sigma_rec = self.fillDiagonal(sigmas_rec, K)
                D_c = dataset.T @ VH.T.conj() @ Sigma_rec @ U.T.conj()
            else:
                D_c = dataset.T @ A_c.T.conj() @ Mat

            D_c = self.normalize(D_c)  # the new atoms are preshaped

            purge_j = np.where(
                (np.abs(A_c) > 1e-3).sum(axis=1) / K
                < self.sparsity / (5 * self.numAtoms)
            )[0]
            for j in range(self.numAtoms):
                if (
                    ssu.configurationNorm(D_c[:, j], self.Phi) < 1e-8
                    or j in purge_j
                ):
                    if verbose:
                        print("purged ", j, "at iteration", t)
                    D_c[:, j] = dataset[np.random.randint(K)]

        if verbose:
            print("computing the final weights...")
        A_c = self.ORMPCholesky(D_c, dataset)

        if verbose:
            elapsed = time.time() - start
            print(
                "duration of the algorithm: ", np.round(elapsed, 2), "seconds"
            )

        diffs = dataset.T - D_c @ A_c
        E = np.diag(diffs.T.conj() @ self.Phi @ diffs).sum().real

        if verbose:
            # print(lossCurve)
            lossCurve = np.append(lossCurve, E)
            plt.figure()
            plt.plot(np.arange(len(lossCurve)), lossCurve)
            plt.title("Loss curve for the KSD algorithm")
            plt.show()

        self.A_c = A_c
        self.D_c = D_c
        return E


def greedyOrder(inputA):
    """Gives a permutation of the dictionary atoms, so that the first atom of
    the dictionary is the one for which the modulus of A[:,0] is maximal;
    then the second atom for which..."""
    import copy

    A = copy.deepcopy(inputA)
    if not np.iscomplexobj(A):
        J = A.shape[0] // 2
        A = ssu.realToComplex(A.T, L=J, can=False).T
    J = A.shape[0]
    K = A.shape[1]
    sort = np.array([], dtype=int)
    k = 0
    if K == 1:
        j0 = np.argmax(np.abs(A[:, k]))
        sort = np.append(sort, j0)
        for j in range(J):
            if j not in sort:
                sort = np.append(sort, j)
        return sort
    # here, K >= 2
    while len(sort) < J and k < K - 1:
        while (
            k < K - 1 and np.argmax(np.abs(A[:, k])) in sort
        ):  # k < K-1 sometimes for K big too long
            k = k + 1
        A[sort, k] = 0
        j0 = np.argmax(np.abs(A[:, k]))
        if j0 not in sort:
            sort = np.append(sort, j0)
    for j in range(J):
        if j not in sort:
            sort = np.append(sort, j)
    return sort, A
