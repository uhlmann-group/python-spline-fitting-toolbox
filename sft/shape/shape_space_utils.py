#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Virginie UHLMANN, Anna SONG

shapespace.py allows one to work in Kendall's shape space (in the 2D case),
extended to spline curves thanks to the Hermitian product Phi.

It defines:
    
    - the three distances d_F, d_P, and rho, also called full, partial,
    or geodesic distances.
    - some operations on the Riemannian manifold, such as
    exp(z,v), log(z,w), geo(z,w),
    and logarithmic or orthogonal projections on a tangent space
      
All functions are defined supposing z and w to be preshapes.

Warning:
    
    Mathematical shapes can only be numerically handled as preshapes.
    
    Hence the `shape' variable designates in fact a preshape
    (centered and normalized configuration), that is one of the many
    representatives of the equivalence class defining the corresponding
    (mathematical) shape.

"""

import numpy as np
from scipy.integrate import quad
from enum import Enum
from sft.spline import basis


class configurationType(Enum):
    LANDMARKS = 0
    CLOSEDBSPLINE = 1
    CLOSEDHSPLINE = 2
    OPENHSPLINE = 3


def getPhi(configType, numAnchors):
    if configType is configurationType.LANDMARKS:
        return np.eye(numAnchors)

    elif configType is configurationType.CLOSEDBSPLINE:
        Phi = np.zeros((numAnchors, numAnchors))

        def aux(k):
            return (
                lambda t: basis.B3().value(numAnchors * (t - 1) - k)
                + basis.B3().value(numAnchors * t - k)
                + basis.B3().value(numAnchors * (t + 1) - k)
            )

        for k in range(numAnchors):
            for l in range(numAnchors):
                u = aux(k)
                v = aux(l)
                Phi[k, l] = innerProductL2(u, v)
        return Phi

    elif (
        configType is configurationType.CLOSEDHSPLINE
        or configurationType.OPENHSPLINE
    ):
        II = np.repeat(np.arange(numAnchors)[:, None], numAnchors, axis=1)
        JJ = np.repeat(np.arange(numAnchors)[None, :], numAnchors, axis=0)

        if configType is configurationType.CLOSEDHSPLINE:
            I = II - JJ
            Phi11 = np.array(
                [
                    [
                        basis.H3().h31PeriodicAutocorrelation(el, numAnchors)
                        for el in row
                    ]
                    for row in I
                ]
            )
            Phi22 = np.array(
                [
                    [
                        basis.H3().h32PeriodicAutocorrelation(el, numAnchors)
                        for el in row
                    ]
                    for row in I
                ]
            )
            Phi12 = np.array(
                [
                    [
                        basis.H3().h3PeriodicCrosscorrelation(el, numAnchors)
                        for el in row
                    ]
                    for row in I
                ]
            )
        else:
            Phi11 = np.array(
                [
                    [
                        basis.H3().h31Autocorrelation(
                            II[j, i], JJ[j, i], numAnchors
                        )
                        for i in range(0, len(II[j]))
                    ]
                    for j in range(0, len(II))
                ]
            )
            Phi22 = np.array(
                [
                    [
                        basis.H3().h32Autocorrelation(
                            II[j, i], JJ[j, i], numAnchors
                        )
                        for i in range(0, len(II[j]))
                    ]
                    for j in range(0, len(II))
                ]
            )
            Phi12 = np.array(
                [
                    [
                        basis.H3().h3Crosscorrelation(
                            II[j, i], JJ[j, i], numAnchors
                        )
                        for i in range(0, len(II[j]))
                    ]
                    for j in range(0, len(II))
                ]
            )

        Phi = np.concatenate(
            (np.hstack((Phi11, Phi12)), np.hstack((Phi12.T, Phi22)))
        )
        return Phi

    else:
        raise NotImplementedError(
            "Unknown configuration type (" + str(configType) + ")."
        )
        return


def innerProductL2(u, v):
    integral = quad(lambda t: u(t) * v(t), 0, 1)
    return integral[0]


def fullDistance(z, w, Phi):  # preshapes
    """Full distance between [z] and [w]
    min_{alpha,theta} ||alpha e^{i theta} z - w||
                    = || w - P_z w || = ||z - P_w z|| \in [0,1]"""
    return np.sqrt(1.0 - np.abs(hermitianProduct(z, w, Phi)) ** 2)


def partialDistance(z, w, Phi):  # preshapes
    """Partial distance between [z] and [w]
    min_{theta}  ||e^{i theta} z - w||"""
    return np.sqrt(2.0 - 2.0 * np.abs(hermitianProduct(z, w, Phi)))


def configurationNorm(z, Phi):
    """Norm of z in C^N w.r.t the Hermitian product Phi."""
    squaredNorm = z.conj().T @ Phi @ z
    return np.sqrt(squaredNorm).real


def exponentialMap(
    z, v, Phi
):  # z preshape, v in C^n referring to a tangent vector
    """Computes the exponential of v at z, that corresponds to a preshape"""
    t = configurationNorm(v, Phi)
    if t < 1e-16:  # catch numerical errors
        return z
    return np.cos(t) * z + v * np.sin(t) / t


def hermitianProduct(z, w, Phi):
    """Hermitian product of z and w in (C^N,Phi)"""
    return z.conj().T @ Phi @ w


def geodesicDistance(z, w, Phi):  # preshapes
    """Geodesic distance between [z] and [w]. """
    aux = np.abs(hermitianProduct(z, w, Phi))
    if aux > 1.0:  # catch numerical errors
        aux = 1.0
    return np.arccos(aux)


def theta(z, w, Phi):  # preshapes
    """Computes the optimal angle theta(z,w) = arg(z* Phi w)
    solution to      min_{theta}  ||e^{i theta} z - w||"""
    return np.angle(hermitianProduct(z, w, Phi))


def logPreshape(z, w, Phi):
    """Computes v = log_z(w) where log is relative to the preshape sphere \S.
    (Requires that z* Phi w > 0, because otherwise
    v does not satisfy Re(z* Phi v) = 0 in order to be on the tangent space
    T_z \S.
    As a consequence, expo(logPre(z,w)) would not be a preshape in \S.)
    """
    ro = geodesicDistance(z, w, Phi)
    return ro / np.sin(ro) * (w - np.cos(ro) * z)


def log(z, w, Phi):  # preshapes
    """Computes a preshape pertaining to the shape (equivalence class) log_[z]
    ([w])
    where log is relative the shape space Sigma."""
    ta = theta(z, w, Phi)
    return logPreshape(z, np.exp(-1j * ta) * w, Phi)


def configurationMean(z, dataType, numAnchors):
    """Computes the mean (landmarks: arithmetic mean; splines: temporal mean)
    of the configuration z in C^N."""
    if (
        dataType is configurationType.LANDMARKS
        or dataType is configurationType.CLOSEDBSPLINE
        or dataType is configurationType.CLOSEDHSPLINE
    ):
        m = np.mean(z[:numAnchors])
    elif dataType is configurationType.OPENHSPLINE:
        m = (
            np.sum(z[1 : numAnchors - 1]) / (numAnchors - 1)
            + (z[0] + z[numAnchors - 1]) / (2 * numAnchors - 2)
            + (z[numAnchors] - z[-1]) / (12 * numAnchors - 12)
        )
    else:
        raise NotImplementedError(f"Unknown data type: {dataType}.")
    return m


def preshape(z, Phi, numAnchors, dataType):
    """Centers and normalizes (i.e. preshapes it) the configuration
    z in C^N."""
    m = configurationMean(z, dataType, numAnchors)
    if (
        dataType is configurationType.LANDMARKS
        or dataType is configurationType.CLOSEDBSPLINE
    ):
        meanSuppressed = z - m
    elif (
        dataType is configurationType.CLOSEDHSPLINE
        or dataType is configurationType.OPENHSPLINE
    ):
        meanSuppressed = z - m * (np.arange(2 * numAnchors) < numAnchors)
    s = configurationNorm(meanSuppressed, Phi)
    return meanSuppressed / s


def meanFrechet(shapes, Phi, numAnchors, dataType):  # preshapes
    """Input: dataset of preshapes called `shapes'.
    Output: Fréchet mean (w.r.t. the distance d_F) of the dataset of shapes.
    It is mathematically a shape, numerically handled as a preshape."""
    SQ = shapes.T @ shapes.conj() @ Phi
    D, V = np.linalg.eig(SQ)
    ds = np.real(D)
    inds = np.argsort(ds)[::-1]
    m = V[:, inds[0]]
    m = preshape(m, Phi, numAnchors, dataType)  # VERY IMPORTANT CONDITION
    return m


def normalizedUnitConfiguration(Phi, numAnchors, dataType):
    if (
        dataType is configurationType.LANDMARKS
        or dataType is configurationType.CLOSEDBSPLINE
    ):
        uu = np.zeros(numAnchors, dtype=complex)
    elif (
        dataType is configurationType.CLOSEDHSPLINE
        or dataType is configurationType.OPENHSPLINE
    ):
        uu = np.zeros(2 * numAnchors, dtype=complex)

    uu[:numAnchors] = 1
    uu = uu / configurationNorm(uu, Phi)
    return uu


def alignShapes(dataset, Phi, numAnchors, dataType, inputMeanShape=None):
    """Optimally rotate the shapes along inputShapeMean if it is given,
    otherwise their Fréchet mean with respect to d_F."""
    K = len(dataset)
    rotated = np.zeros_like(dataset)
    if inputMeanShape is None:
        shapeMean = meanFrechet(dataset, Phi, numAnchors, dataType)
    else:
        shapeMean = inputMeanShape
    for k in range(K):
        ta = theta(dataset[k], shapeMean, Phi)
        rotated[k] = np.exp(1j * ta) * dataset[k]
    return rotated


def geodesicPath(z, w, Phi, numSteps=5):  # preshapes
    """Returns elements regularly spaced along the geodesic curve joining
    z to w (preshapes)."""
    ro = geodesicDistance(z, w, Phi)
    steps = np.arange(numSteps + 1) / numSteps

    ta = theta(z, w, Phi)
    path = (
        1
        / np.sin(ro)
        * (
            np.sin((1 - steps[:, None]) * ro) * np.exp(1j * ta) * z
            + np.sin(steps[:, None] * ro) * w
        )
    )
    return path


def projectTangentSpace(m, z, Phi):  # preshapes
    """Orthogonally project z onto tangent space at m
    (considering the ambient space)"""
    if np.abs(hermitianProduct(m, z, Phi)) < 1e-2:
        return z
    ta = theta(z, m, Phi)
    v_proj = np.exp(1j * ta) * (z - hermitianProduct(m, z, Phi) * m)
    return v_proj


def realToComplex(realConfigurations):
    """Converts a collection of real configuration in R^{2N} in a
    collection of complex configurations in C^N."""
    return realConfigurations[:, :, 0] + 1j * realConfigurations[:, :, 1]


def complexToReal(complexConfigurations):
    """Converts a collection of complex configurations in C^N to a
    collection of real configuration in R^{2N}."""
    return np.stack(
        (complexConfigurations.real, complexConfigurations.imag), axis=2
    )


def getPreshapes(complexConfigurations, numAnchors, Phi, dataType):
    """Same as preshape(), but for several horizontally stacked
    configurations."""
    if (
        dataType is configurationType.LANDMARKS
        or dataType is configurationType.CLOSEDBSPLINE
    ):
        shapes = np.zeros(
            (complexConfigurations.shape[0], numAnchors), dtype=complex
        )
    elif (
        dataType is configurationType.CLOSEDHSPLINE
        or dataType is configurationType.OPENHSPLINE
    ):
        shapes = np.zeros(
            (complexConfigurations.shape[0], 2 * numAnchors), dtype=complex
        )

    for k in range(complexConfigurations.shape[0]):
        shapes[k] = preshape(
            complexConfigurations[k], Phi, numAnchors, dataType
        )
    return shapes
