import copy
import itertools
import multiprocessing
import warnings

import numpy as np
from matplotlib.path import Path
from scipy import integrate


class SplineCurve:
    _wrong_dimension_message = "It looks like coefs is a 2D array with second " \
                            "dimension different than two. I don't know how " \
                            "to handle this yet."
    _wrong_array_size_message = "It looks like coefs is neither a 1 nor a 2D " \
                            "array. I don't know how to handle this yet."
    _no_coefs_message = "This model doesn't have any coefficients."
    _unimplemented_message = "This function is not implemented."

    # A cache from pair (M, sampling_rate) to phi matrices

    # In the case of the usual sampling, phi can be constructed once for
    # several SplineCurve and stored, given:
    #
    #  - M: the number of control points
    #  - the closeness of the curve
    #  - the basis to use
    #  - the contour size to use
    #  - the derivative order
    #
    # We maintain a class dictionary for this use case: _phi
    #
    # In the case of even sampling, phi is constructed using range of
    # parameters (_even_sampling_ts_ranges) which depends on the geometry
    # of a given SplineCurve, the contour_size and a over_sampling_factor.
    #
    # Hence we have to maintain another dictionary on each instance
    # here: _phis_even

    _phi = dict()

    def __init__(self, M, basis, closed):
        if M >= basis.support:
            self._M = M
        else:
            raise RuntimeError(
                "M must be greater or equal than the "
                "spline basis support size."
            )

        self._basis = basis
        self._closed = closed
        self._coefs = None

        # Caches to store values which are instances dependent
        self._phis_even = dict()
        self._even_sampling_ts_ranges = dict()

        # Used to know if a point is inside the curve or not
        self._matplotlib_path = None

    @property
    def M(self):
        return self._M

    @M.setter
    def M(self, value):
        raise RuntimeError(
            f"Can't override {self.__class__.__name__}.M"
        )

    @property
    def basis(self):
        return self._basis

    @basis.setter
    def basis(self, value):
        raise RuntimeError(
            f"Can't override {self.__class__.__name__}.basis"
        )

    @property
    def closed(self):
        return self._closed

    @closed.setter
    def closed(self, value):
        raise RuntimeError(
            f"Can't override {self.__class__.__name__}.closed"
        )

    @property
    def coefs(self):
        return self._coefs

    @coefs.setter
    def coefs(self, value):
        raise RuntimeError(
            f"Can't override {self.__class__.__name__}.coefs"
        )

    @property
    def halfSupport(self):
        return self.basis.support / 2.0

    @halfSupport.setter
    def halfSupport(self, value):
        raise RuntimeError(
            f"Can't override {self.__class__.__name__}.halfSupport"
        )

    @property
    def halfSupport(self):
        return self.basis.support / 2.0

    @halfSupport.setter
    def halfSupport(self, value):
        raise RuntimeError(
            f"Can't override {self.__class__.__name__}.halfSupport"
        )

    def _get_even_sampling_t_range(self, range_length, over_sampling_factor=10):
        """
        Return the range of parameters to have evenly space points sampled
        on the curve.

        The length of the curve is used to performed a look-up for good
        parameters using some over sampling.

        @param range_length: the length of the parameter range to return.
        @param over_sampling_factor: the number of parameters to oversample for
        a given parameters. The larger, the more accurate the returned range but
        the slower to compute.
        """

        n_over_sample = range_length * over_sampling_factor

        key = (range_length, over_sampling_factor)

        # Checking for the parameter range in the cache
        ts_even = self._even_sampling_ts_ranges.get(key, None)

        if ts_even is None:
            # We first oversample points on the curve to be able to find
            # good parameter for a requested length
            ts_over_sampled = np.linspace(0, float(self.M), num=n_over_sample, endpoint=True)
            phi_prime = self._get_phi(n_over_sample, derivative=True)

            d_lengths = np.linalg.norm(phi_prime @ self.coefs, axis=1)

            # Length at each parameter
            lengths = integrate.cumulative_trapezoid(d_lengths, ts_over_sampled)

            # Map the length in [0, M] to perform the search
            norm_lengths = self.M * ((lengths - lengths.min()) /
                                     (lengths.max() - lengths.min()))

            ts = np.linspace(0, float(self.M), num=range_length, endpoint=True)

            # We simply rely on the lengths to get the parameters for the
            # even sampling here
            ts_even = ts_over_sampled[np.searchsorted(norm_lengths, ts)]
            self._even_sampling_ts_ranges[key] = ts_even

        return ts_even

    def _get_phi(self, range_length, derivative=False, even=False):
        """
        Return the phi matrix.

        phi has potentially has been cached previously

        @param range_length: the length of the parameter range to return.
        @derivative: If True, the derivative of the basis is used to construct
        phi.
        @even: If True, phi is constructed with a range of parameters
        for evenly sampled points on the curve.
        """
        key = (self.M, self.closed, self.basis.__class__, range_length, derivative)

        phis_map = self._phis_even if even else self._phi

        phi = phis_map.get(key, None)

        if phi is None:
            # We compute it once and we store it
            if even:
                ts = self._get_even_sampling_t_range(range_length)
            else:
                if self.closed:
                    ts = np.linspace(0, float(self.M), num=range_length, endpoint=False)
                else:
                    ts = np.linspace(0, float(self.M - 1), num=range_length, endpoint=True)
                
            if self.closed:
                wrapped_indices = np.array(
                    [
                        [
                            self._wrapIndex(t, k)
                            for k in range(self.M)
                        ]
                        for t in ts
                    ]
                )
            else:
                wrapped_indices = np.array(
                    [
                        [
                            t - (k - self.halfSupport)
                            for k in range(self.M+int(self.basis.support))
                        ]
                        for t in ts
                    ]
                )
            vfunc = np.vectorize(self.basis.firstDerivativeValue
                                 if derivative else self.basis.value)
            phi = vfunc(wrapped_indices)
            phis_map[key] = phi
        return phi

    def sample(self, sampling_rate, even=False):
        """
        Sample contour points on the curve.

        @param sampling_rate: the number of points sampled between two knots.
        @param even: if True, points on the curve are sampled evenly.
        """
        contour_size = self.M * sampling_rate
        if self.closed:
            contour_size = self.M * sampling_rate
        else:
            contour_size = (sampling_rate * (self.M - 1)) + 1

        phi = self._get_phi(range_length=contour_size, even=even)
        contour_points = phi @ self.coefs
        return contour_points

    def draw(self, dimensions):
        if self.coefs is None:
            raise RuntimeError(self._no_coefs_message)

        if len(self.coefs.shape) != 2 or self.coefs.shape[1] != 2:
            raise RuntimeError("draw() can only be used with 2D curves.")

        if not (self.closed):
            raise RuntimeError("draw() can only be used with closed curves.")

        warnings.warn("draw() will take ages, go get yourself a coffee.")

        if len(dimensions) != 2:
            raise RuntimeError("dimensions must be a triplet.")

        xvals = range(dimensions[1])
        yvals = range(dimensions[0])
        pointsList = list(itertools.product(xvals, yvals))

        vals = np.asarray([self.isInside(p) for p in pointsList])

        area = np.zeros(dimensions, dtype=np.int8)
        for i in range(0, len(pointsList)):
            p = pointsList[i]
            area[p[1], p[0]] = int(255 * vals[i])

        return area

    def windingNumber(self, t):
        r = self.parameterToWorld(t)
        dr = self.parameterToWorld(t, dt=True)

        r2 = np.linalg.norm(r) ** 2
        val = (1.0 / r2) * (r[0] * dr[1] - r[1] * dr[0])
        return val

    def isInside(self, points):
        """
        For each point in points, return 1 if the points is inside the curve,
        0 otherwise.

        @param points: (n, 2) array of points.
        """
        points = np.asarray(points)

        if self._matplotlib_path is None:
            polygon = self.sample(sampling_rate=100, even=True)
            self._matplotlib_path = Path(polygon, closed=self.closed)

        if len(points.shape) == 1:
            points = points[np.newaxis, ...]
        return 1 * self._matplotlib_path.contains_points(points)

    def setCoefsFromKnots(self, knots):
        knots = np.array(knots)

        if self.closed:
            filter = self.basis.filterPeriodic
        else:
            knots = np.vstack((knots,knots[-1]))
            knots = np.vstack((knots,knots[-1]))
            knots = np.vstack((knots[0],knots))
            knots = np.vstack((knots[0],knots))
            filter = self.basis.filterSymmetric

        if len(knots.shape) > 2:
            raise RuntimeError(self._wrong_array_size_message)
        elif len(knots.shape) == 1:
            self._coefs = filter(knots)
        elif len(knots.shape) == 2:
            if knots.shape[1] != 2:
                raise RuntimeError(self._wrong_dimension_message)
            else:
                coefsX = filter(knots[:, 0])
                coefsY = filter(knots[:, 1])
                self._coefs = np.hstack(
                    (
                        np.array([coefsX]).transpose(),
                        np.array([coefsY]).transpose(),
                    )
                )

    def setCoefsFromDenseContour(self, contourPoints):
        N = len(contourPoints)

        if self.closed:
            phi = np.zeros((N, self.M))
        else:
            phi = np.zeros((N, self.M + int(self.basis.support)))

        if len(contourPoints.shape) == 1:
            r = np.zeros((N))
        elif len(contourPoints.shape) == 2:
            if contourPoints.shape[1] == 2:
                r = np.zeros((N, 2))

        if self.closed:
            samplingRate = int(N / self.M)
            extraPoints = N % self.M
        else:
            samplingRate = int(N / (self.M - 1))
            extraPoints = N % (self.M - 1)

        for i in range(0, N):
            r[i] = contourPoints[i]

            if i/samplingRate < extraPoints:
                t = i/(samplingRate + 1.0)
            else:
                t = i/samplingRate

            if self.closed:
                for k in range(0, self.M):
                    tval = self._wrapIndex(t, k)
                    if -self.halfSupport < tval < self.halfSupport:
                        basisFactor = self.basis.value(tval)
                    else:
                        basisFactor = 0.0
                    phi[i, k] += basisFactor
            else:
                for k in range(0, self.M + int(self.basis.support)):
                    tval = t - (k - self.halfSupport)
                    if -self.halfSupport < tval < self.halfSupport:
                        basisFactor = self.basis.value(tval)
                    else:
                        basisFactor = 0.0
                    phi[i, k] += basisFactor

        if len(contourPoints.shape) == 1:
            c = np.linalg.lstsq(phi, r, rcond=None)

            if self.closed:
                self._coefs = np.zeros([self.M])
                for k in range(0, self.M):
                    self._coefs[k] = c[0][k]
            else:
                self._coefs = np.zeros([self.M + int(self.basis.support)])
                for k in range(0, self.M + int(self.basis.support)):
                    self._coefs[k] = c[0][k]

        elif len(contourPoints.shape) == 2:
            if contourPoints.shape[1] == 2:
                cX = np.linalg.lstsq(phi, r[:, 0], rcond=None)
                cY = np.linalg.lstsq(phi, r[:, 1], rcond=None)

                if self.closed:
                    self._coefs = np.zeros([self.M, 2])
                    for k in range(0, self.M):
                        self._coefs[k] = np.array([cX[0][k], cY[0][k]])
                else:
                    self._coefs = np.zeros([self.M + int(self.basis.support), 2])
                    for k in range(0, self.M + int(self.basis.support)):
                        self._coefs[k] = np.array([cX[0][k], cY[0][k]])

    def setCoefsFromBinaryMask(self, binaryMask):
        from skimage import measure

        contours = measure.find_contours(binaryMask, 0)

        if len(contours) > 1:
            raise RuntimeWarning(
                "Multiple objects were found on the binary mask. "
                "Only the first one will be processed."
            )

        self.setCoefsFromDenseContour(contours[0])

    def arcLength(self, t0, tf=None):
        if t0 == tf:
            return 0.0

        if tf is None:
            if self.closed:
                tf = self.M
            else:
                tf = self.M - 1

        if t0 > tf:
            temp = tf
            tf = t0
            t0 = temp

        integral = integrate.quad(
            lambda t: np.linalg.norm(self.parameterToWorld(t, dt=True)),
            t0,
            tf,
            epsabs=1e-6,
            epsrel=1e-6,
            maxp1=50,
            limit=100,
        )

        return integral[0]

    def lengthToParameterRecursion(
        self, s, currentValue, lowerBound, upperBound, precisionDecimals=4
    ):
        midPoint = lowerBound + (upperBound - lowerBound) / 2
        midPointLength = currentValue + self.arcLength(lowerBound, midPoint)

        if (
            np.round(currentValue, precisionDecimals)
            == np.round(midPointLength, precisionDecimals)
        ) or (
            np.round(s, precisionDecimals)
            == np.round(midPointLength, precisionDecimals)
        ):
            return np.round(midPoint, precisionDecimals)

        elif np.round(s, precisionDecimals) < np.round(
            midPointLength, precisionDecimals
        ):
            return self.lengthToParameterRecursion(
                s, currentValue, lowerBound, midPoint, precisionDecimals
            )
        else:
            return self.lengthToParameterRecursion(
                s, midPointLength, midPoint, upperBound, precisionDecimals
            )

    def lengthToParameter(self, s):
        if self.closed:
            return self.lengthToParameterRecursion(s, 0, 0, self.M)
        else:
            return self.lengthToParameterRecursion(s, 0, 0, self.M - 1)

    def sampleArcLength(self, numSamples):
        warnings.warn(f"{self.__class__.__name__}.sampleArcLength is deprecated."
                      f" Please use {self.__class__.__name__}.sample with "
                      f"even=true", DeprecationWarning)

        if self.coefs is None:
            raise RuntimeError(self._no_coefs_message)

        if len(self.coefs.shape) == 1 or (
            len(self.coefs.shape) == 2 and self.coefs.shape[1] == 2
        ):
            if self.closed:
                N = numSamples
            else:
                N = numSamples - 1
            L = self.arcLength(0)

            ts = [0]
            for n in range(1, N):
                s = n * L / N
                t = self.lengthToParameter(s)
                ts.append(t)
            if self.closed:
                ts.append(self.M)
            else:
                ts.append(self.M - 1)

            curve = np.array([self.parameterToWorld(t) for t in ts])
            if len(self.coefs.shape) == 1:
                curve = curve[~np.all(curve == 0)]
            else:
                curve = curve[~np.all(curve == 0, axis=1)]

        else:
            raise RuntimeError(self._wrong_array_size_message)

        return np.stack(curve)

    def parameterToWorld(self, t, dt=False):
        if self.coefs is None:
            raise RuntimeError(SplineCurve._no_coefs_message)

        if self.closed:
            ts = np.array([self._wrapIndex(t, k) for k in range(self.M)])
        else:
            ts = np.array([t - (k - self.halfSupport) for k in range(self.M + int(self.basis.support))])
        f = self.basis.firstDerivativeValue if dt else self.basis.value
        vec_f = np.vectorize(f)
        phi = vec_f(ts)
        value = phi @ self.coefs
        return value

    def _wrapIndex(self, t, k):
        t_left = t - self.halfSupport
        t_right = t + self.halfSupport
        if k < t_left <= k + self.M <= t_right:
            return t - (k + self.M)
        if t_left <= k - self.M <= t_right < k:
            return t - (k - self.M)
        return t - k

    def centroid(self):
        return self.coefs[:self.M].mean(axis=0)

    def translate(self, translationVector):
        self._coefs[:self.M] += translationVector

    def scale(self, scalingFactor):
        centroid = self.centroid()
        centered_coefs = self.coefs[:self.M] - centroid
        self._coefs[:self.M] = centroid + scalingFactor * centered_coefs

    def rotate(self, rotationMatrix):
        self._coefs[:self.M] = np.matmul(rotationMatrix, self.coefs[:self.M])


class HermiteSplineCurve(SplineCurve):
    coefTangentMismatchMessage = (
        "It looks like coefs and tangents have different shapes."
    )

    def __init__(self, M, basis, closed):
        if not basis.multigenerator:
            raise RuntimeError(
                "It looks like you are trying to use a single basis to "
                "build a multigenerator spline model."
            )

        SplineCurve.__init__(self, M, basis, closed)
        self._tangents = None

    @property
    def tangents(self):
        return self._tangents

    @tangents.setter
    def tangents(self, value):
        raise RuntimeError(
            f"Can't override {self.__class__.__name__}.tangents"
        )

    def setCoefsFromKnots(self, knots, tangentAtKnots):
        knots = np.array(knots)
        tangentAtKnots = np.array(tangentAtKnots)

        if knots.shape != tangentAtKnots.shape:
            raise RuntimeError(self.coefTangentMismatchMessage)

        if len(knots.shape) > 2:
            raise RuntimeError(SplineCurve._wrong_array_size_message)
        if len(knots.shape) == 2 and knots.shape[1] != 2:
            raise RuntimeError(SplineCurve._wrong_dimension_message)

        self._coefs = knots
        self._tangents = tangentAtKnots

    def setCoefsFromDenseContour(self, contourPoints, tangentAtPoints):
        # TODO
        raise NotImplementedError(SplineCurve._unimplemented_message)

    def setCoefsFromBinaryMask(self, binaryMask):
        # TODO
        raise NotImplementedError(SplineCurve._unimplemented_message)

    def parameterToWorld(self, t, dt=False):
        if self.coefs is None:
            raise RuntimeError(SplineCurve._no_coefs_message)

        value = 0.0
        for k in range(0, self.M):
            if self.closed:
                tval = self._wrapIndex(t, k)
            else:
                tval = t - k
            if -self.halfSupport < tval < self.halfSupport:
                if dt:
                    splineValue = self.basis.firstDerivativeValue(tval)
                else:
                    splineValue = self.basis.value(tval)
                value += (
                    self.coefs[k] * splineValue[0]
                    + self.tangents[k] * splineValue[1]
                )
        return value

    def scale(self, scalingFactor):
        SplineCurve.scale(self, scalingFactor)
        self._tangents[: self.M] *= scalingFactor

    def rotate(self, rotationMatrix):
        SplineCurve.rotate(self, rotationMatrix)
        self._tangents[: self.M] = np.matmul(
            rotationMatrix, self.tangents[: self.M]
        )


class SplineCurveDebug(SplineCurve):
    """ A interface using unfolded/inlined computation for sampling.

    To be used for debugging/inspection purposes.

    Implementations originate from commit d92ff02c.
    """

    def sample(self, samplingRate, cpuCount=1):
        if self.coefs is None:
            raise RuntimeError(self._no_coefs_message)

        if len(self.coefs.shape) == 1 or (
            len(self.coefs.shape) == 2 and self.coefs.shape[1] == 2
        ):
            if self.closed:
                N = samplingRate * self.M
            else:
                N = (samplingRate * (self.M - 1)) + 1

            if cpuCount == 1:
                curve = [
                    self.parameterToWorld(float(i) / float(samplingRate))
                    for i in range(0, N)
                ]
            else:
                cpuCount = np.min((cpuCount, multiprocessing.cpu_count()))
                with multiprocessing.Pool(cpuCount) as pool:
                    iterable = [float(i) / float(samplingRate) for i in range(0, N)]
                    res = pool.map(self.parameterToWorld, iterable)

                curve = np.stack(res)
                if len(self.coefs.shape) == 1:
                    curve = curve[~np.all(curve == 0)]
                else:
                    curve = curve[~np.all(curve == 0, axis=1)]

        else:
            raise RuntimeError(self._wrong_array_size_message)

        return np.stack(curve)

    def parameterToWorld(self, t, dt=False):
        if self.coefs is None:
            raise RuntimeError(SplineCurve._no_coefs_message)

        value = 0.0
        if self.closed:
            for k in range(0, self.M):
                tval = self._wrapIndex(t, k)
                if -self.halfSupport < tval < self.halfSupport:
                    if dt:
                        splineValue = self.basis.firstDerivativeValue(tval)
                    else:
                        splineValue = self.basis.value(tval)
                    value += self.coefs[k] * splineValue
        else:
            for k in range(0, self.M + int(self.basis.support)):
                tval = t - (k - self.halfSupport)
                if -self.halfSupport < tval < self.halfSupport:
                    if dt:
                        splineValue = self.basis.firstDerivativeValue(tval)
                    else:
                        splineValue = self.basis.value(tval)
                    value += self.coefs[k] * splineValue
        return value

