import numpy as np
import math
import abc
import mpmath

# TODO: local refinement filters
# TODO: quadratic prefilters


class Basis(abc.ABC):
    _unimplemented_message = "This method is not implemented."

    def __init__(self, multigenerator, support):
        self.multigenerator = multigenerator
        self._support = support

    @abc.abstractmethod
    def value(self, x):
        raise NotImplementedError(Basis._unimplemented_message)

    @abc.abstractmethod
    def firstDerivativeValue(self, x):
        raise NotImplementedError(Basis._unimplemented_message)

    @abc.abstractmethod
    def secondDerivativeValue(self, x):
        raise NotImplementedError(Basis._unimplemented_message)

    @abc.abstractmethod
    def filterSymmetric(self, s):
        raise NotImplementedError(Basis._unimplemented_message)

    @abc.abstractmethod
    def filterPeriodic(self, s):
        raise NotImplementedError(Basis._unimplemented_message)

    @abc.abstractmethod
    def refinementMask(self):
        raise NotImplementedError(Basis._unimplemented_message)

    @property
    def support(self):
        return self._support

    @support.setter
    def support(self, value):
        raise RuntimeError(f"Can't override {self.__class__.__name__}.support")


class B1(Basis):
    def __init__(self):
        Basis.__init__(self, multigenerator=False, support=2.0)

    def value(self, x):
        val = 0.0
        if 0 <= abs(x) < 1:
            val = 1.0 - abs(x)
        return val

    def firstDerivativeValue(self, x):
        val = 0.0
        if -1.0 <= x < 0:
            val = -1.0
        elif 0 <= x <= 1:
            val = 1.0
        return val

    def secondDerivativeValue(self, x):
        raise RuntimeError("B1 isn't twice differentiable.")

    def filterSymmetric(self, s):
        return s

    def filterPeriodic(self, s):
        return s

    def refinementMask(self):
        order = int(self.support)
        mask = np.zeros((order + 1))
        multinomial(order, 2, np.zeros(2), 0, 2, order, mask)
        return mask


class B2(Basis):
    def __init__(self):
        Basis.__init__(self, multigenerator=False, support=3.0)

    def value(self, x):
        val = 0.0
        if -1.5 <= x <= -0.5:
            val = 0.5 * (x ** 2) + 1.5 * x + 1.125
        elif -0.5 < x <= 0.5:
            val = -x * x + 0.75
        elif 0.5 < x <= 1.5:
            val = 0.5 * (x ** 2) - 1.5 * x + 1.125
        return val

    def firstDerivativeValue(self, x):
        val = 0.0
        if -1.5 <= x <= -0.5:
            val = x + 1.5
        elif -0.5 < x <= 0.5:
            val = -2.0 * x
        elif 0.5 < x <= 1.5:
            val = x - 1.5
        return val

    def secondDerivativeValue(self, x):
        val = 0.0
        if -1.5 <= x <= -0.5:
            val = 1.0
        elif -0.5 < x <= 0.5:
            val = -2.0
        elif 0.5 < x <= 1.5:
            val = 1.0
        return val

    def refinementMask(self):
        order = int(self.support)
        mask = np.zeros((order + 1))
        multinomial(order, 2, np.zeros(2), 0, 2, order, mask)
        return mask

    def filterSymmetric(self, s):
        raise NotImplementedError()

    def filterPeriodic(self, s):
        raise NotImplementedError()


class B3(Basis):
    def __init__(self):
        Basis.__init__(self, multigenerator=False, support=4.0)

    def value(self, x):
        val = 0.0
        if 0 <= abs(x) < 1:
            val = 2.0 / 3.0 - (abs(x) ** 2) + (abs(x) ** 3) / 2.0
        elif 1 <= abs(x) <= 2:
            val = ((2.0 - abs(x)) ** 3) / 6.0
        return val

    def firstDerivativeValue(self, x):
        val = 0.0
        if 0 <= x < 1:
            val = -2.0 * x + 1.5 * x * x
        elif -1 < x < 0:
            val = -2.0 * x - 1.5 * x * x
        elif 1 <= x <= 2:
            val = -0.5 * ((2.0 - x) ** 2)
        elif -2 <= x <= -1:
            val = 0.5 * ((2.0 + x) ** 2)
        return val

    def secondDerivativeValue(self, x):
        val = 0.0
        if 0 <= x < 1:
            val = -2.0 + 3.0 * x
        elif -1 < x < 0:
            val = -2.0 - 3.0 * x
        elif 1 <= x <= 2:
            val = 2.0 - x
        elif -2 <= x <= -1:
            val = 2.0 + x
        return val

    def filterSymmetric(self, s):
        M = len(s)
        pole = -2.0 + np.sqrt(3.0)

        cp = np.zeros(M)
        eps = 1e-8
        k0 = np.min(
            ((2 * M) - 2, int(np.ceil(np.log(eps) / np.log(np.abs(pole)))))
        )
        for k in range(0, k0):
            k = k % (2 * M - 2)
            if k >= M:
                val = s[2 * M - 2 - k]
            else:
                val = s[k]
            cp[0] += val * (pole ** k)
        cp[0] *= 1.0 / (1.0 - (pole ** (2 * M - 2)))

        for k in range(1, M):
            cp[k] = s[k] + pole * cp[k - 1]

        cm = np.zeros(M)
        cm[M - 1] = cp[M - 1] + (pole * cp[M - 2])
        cm[M - 1] *= pole / ((pole ** 2) - 1)
        for k in range(M - 2, -1, -1):
            cm[k] = pole * (cm[k + 1] - cp[k])

        c = cm * 6.0

        c[np.where(abs(c) < eps)] = 0.0
        return c

    def filterPeriodic(self, s):
        M = len(s)
        pole = -2.0 + np.sqrt(3.0)

        cp = np.zeros(M)
        for k in range(0, M):
            cp[0] += s[(M - k) % M] * (pole ** k)
        cp[0] *= 1.0 / (1.0 - (pole ** M))

        for k in range(1, M):
            cp[k] = s[k] + pole * cp[k - 1]

        cm = np.zeros(M)
        for k in range(0, M):
            cm[M - 1] += (pole ** k) * cp[k]
        cm[M - 1] *= pole / (1.0 - (pole ** M))
        cm[M - 1] += cp[M - 1]
        cm[M - 1] *= -pole

        for k in range(M - 2, -1, -1):
            cm[k] = pole * (cm[k + 1] - cp[k])

        c = cm * 6.0

        eps = 1e-8
        c[np.where(abs(c) < eps)] = 0.0
        return c

    def refinementMask(self):
        order = int(self.support)
        mask = np.zeros((order + 1))
        multinomial(order, 2, np.zeros((2)), 0, 2, order, mask)
        return mask


class EM(Basis):
    def __init__(self, M, alpha):
        Basis.__init__(self, multigenerator=False, support=3.0)
        self.M = M
        self.alpha = alpha

    def value(self, x):
        x += self.support / 2.0
        L = (np.sin(np.pi / self.M) / (np.pi / self.M)) ** (-2)

        val = 0.0
        if 0 <= x < 1:
            val = (
                2.0
                * np.sin(self.alpha * 0.5 * x)
                * np.sin(self.alpha * 0.5 * x)
            )
        elif 1 <= x < 2:
            val = (
                np.cos(self.alpha * (x - 2))
                + np.cos(self.alpha * (x - 1))
                - 2.0 * np.cos(self.alpha)
            )
        elif 2 <= x <= 3:
            val = (
                2.0
                * np.sin(self.alpha * 0.5 * (x - 3))
                * np.sin(self.alpha * 0.5 * (x - 3))
            )

        return (L * val) / (self.alpha * self.alpha)

    def firstDerivativeValue(self, x):
        x += self.support / 2.0
        L = (np.sin(np.pi / self.M) / (np.pi / self.M)) ** (-2)

        val = 0.0
        if 0 <= x <= 1:
            val = self.alpha * np.sin(self.alpha * x)
        elif 1 < x <= 2:
            val = self.alpha * (
                np.sin(self.alpha * (1 - x)) + np.sin(self.alpha * (2 - x))
            )
        elif 2 < x <= 3:
            val = self.alpha * np.sin(self.alpha * (x - 3))

        return (L * val) / (self.alpha * self.alpha)

    def secondDerivativeValue(self, x):
        x += self.support / 2.0
        L = (np.sin(np.pi / self.M) / (np.pi / self.M)) ** (-2)

        val = 0.0
        if 0 <= x <= 1:
            val = self.alpha * self.alpha * np.cos(self.alpha * x)
        elif 1 < x <= 2:
            val = (
                self.alpha
                * self.alpha
                * (
                    -np.cos(self.alpha * (1 - x))
                    - np.cos(self.alpha * (2 - x))
                )
            )
        elif 2 < x <= 3:
            val = self.alpha * self.alpha * np.cos(self.alpha * (x - 3))

        return (L * val) / (self.alpha * self.alpha)

    def filterSymmetric(self, s):
        self.M = len(s)
        b0 = self.value(0)
        b1 = self.value(1)
        pole = (-b0 + np.sqrt(2.0 * b0 - 1.0)) / (1.0 - b0)

        cp = np.zeros(self.M)
        eps = 1e-8
        k0 = np.min(
            (
                (2 * self.M) - 2,
                int(np.ceil(np.log(eps) / np.log(np.abs(pole)))),
            )
        )
        for k in range(0, k0):
            k = k % (2 * self.M - 2)
            if k >= self.M:
                val = s[2 * self.M - 2 - k]
            else:
                val = s[k]
            cp[0] += val * (pole ** k)
        cp[0] *= 1.0 / (1.0 - (pole ** (2 * self.M - 2)))

        for k in range(1, self.M):
            cp[k] = s[k] + pole * cp[k - 1]

        cm = np.zeros(self.M)
        cm[self.M - 1] = cp[self.M - 1] + (pole * cp[self.M - 2])
        cm[self.M - 1] *= pole / ((pole * pole) - 1)
        for k in range(self.M - 2, -1, -1):
            cm[k] = pole * (cm[k + 1] - cp[k])

        c = cm / b1

        c[np.where(np.abs(c) < eps)] = 0.0
        return c

    def filterPeriodic(self, s):
        self.M = len(s)
        b0 = self.value(0)
        pole = (-b0 + np.sqrt(2.0 * b0 - 1.0)) / (1.0 - b0)

        cp = np.zeros(self.M)
        cp[0] = s[0]
        for k in range(0, self.M):
            cp[0] += s[k] * (pole ** (self.M - k))
        cp[0] *= 1.0 / (1.0 - (pole ** self.M))

        for k in range(1, self.M):
            cp[k] = s[k] + (pole * cp[k - 1])

        cm = np.zeros(self.M)
        cm[self.M - 1] = cp[self.M - 1]
        for k in range(0, self.M - 1):
            cm[self.M - 1] += cp[k] * (pole ** (k + 1))
        cm[self.M - 1] *= 1.0 / (1.0 - (pole ** self.M))
        cm[self.M - 1] *= (1 - pole) ** 2

        for k in range(self.M - 2, -1, -1):
            cm[k] = (pole * cm[k + 1]) + (((1 - pole) ** 2) * cp[k])

        c = cm

        eps = 1e-8
        c[np.where(np.abs(c) < eps)] = 0.0
        return c

    def refinementMask(self):
        order = int(self.support)
        mask = np.zeros((order + 1))

        denominator = 2.0 ** (order - 1.0)
        mask[0] = 1.0 / denominator
        mask[1] = (2.0 * np.cos(self.alpha) + 1.0) / denominator
        mask[2] = mask[1]
        mask[3] = 1.0 / denominator

        return mask


class Keys(Basis):
    def __init__(self):
        Basis.__init__(self, multigenerator=False, support=4.0)

    def value(self, x):
        val = 0.0
        if 0 <= np.abs(x) <= 1:
            val = (
                (3.0 / 2.0) * (np.abs(x) ** 3)
                - (5.0 / 2.0) * (np.abs(x) ** 2)
                + 1
            )
        elif 1 < np.abs(x) <= 2:
            val = (
                (-1.0 / 2.0) * (np.abs(x) ** 3)
                + (5.0 / 2.0) * (np.abs(x) ** 2)
                - 4.0 * np.abs(x)
                + 2.0
            )
        return val

    def firstDerivativeValue(self, x):
        val = 0.0
        if 0 <= x <= 1:
            val = x * (4.5 * x - 5.0)
        elif -1 <= x < 0:
            val = -x * (4.5 * x + 5.0)
        elif 1 < x <= 2:
            val = -1.5 * x * x + 5.0 * x - 4.0
        elif -2 <= x < -1:
            val = 1.5 * x * x + 5.0 * x + 4.0
        return val

    def secondDerivativeValue(self, x):
        val = 0.0
        if 0 <= x <= 1:
            val = 9.0 * x - 5.0
        elif -1 <= x < 0:
            val = -9.0 * x - 5.0
        elif 1 < x <= 2:
            val = -3.0 * x + 5.0
        elif -2 <= x < -1:
            val = 3.0 * x + 5.0
        return val

    def filterSymmetric(self, s):
        return s

    def filterPeriodic(self, s):
        return s

    def refinementMask(self):
        raise NotImplementedError()


class H3(Basis):
    def __init__(self):
        Basis.__init__(self, multigenerator=True, support=2.0)

    def value(self, x):
        return np.array([self.h31(x), self.h32(x)])

    def h31(self, x):
        val = 0.0
        if 0 <= x <= 1:
            val = (1.0 + (2.0 * x)) * (x - 1) * (x - 1)
        elif 0 > x >= -1:
            val = (1.0 - (2.0 * x)) * (x + 1) * (x + 1)
        return val

    def h32(self, x):
        val = 0.0
        if 0 <= x <= 1:
            val = x * (x - 1) * (x - 1)
        elif -1 <= x < 0:
            val = x * (x + 1) * (x + 1)
        return val

    def firstDerivativeValue(self, x):
        return np.array([self.h31prime(x), self.h32prime(x)])

    def h31prime(self, x):
        val = 0.0
        if 0 <= x <= 1:
            val = 6.0 * x * (x - 1.0)
        elif -1 <= x < 0:
            val = -6.0 * x * (x + 1.0)
        return val

    def h32prime(self, x):
        val = 0.0
        if 0 <= x <= 1:
            val = 3.0 * x * x - 4.0 * x + 1
        elif -1 <= x < 0:
            val = 3.0 * x * x + 4.0 * x + 1
        return val

    def secondDerivativeValue(self, x):
        raise RuntimeError("H3 isn't twice differentiable.")

    def h31Autocorrelation(self, i, j, M):
        if M < self.support:
            raise ValueError(
                "Cannot compute h31Autocorrelation for M < "
                + str(self.support)
            )

        val = 0.0
        if np.abs(i - j) == 1:
            val = 9.0 / ((M - 1.0) * 70.0)
        elif i == j:
            if (i == 0) or (i == M - 1):
                val = 13.0 / ((M - 1.0) * 35.0)
            else:
                val = 26.0 / ((M - 1.0) * 35.0)

        return val

    def h31PeriodicAutocorrelation(self, n, M):
        if M < self.support:
            raise ValueError(
                "Cannot compute h31PeriodicAutocorrelation for M < "
                + str(self.support)
            )

        nmod = np.mod(n, M)
        val = 0.0
        if nmod == 0:
            val = 26.0 / (M * 35.0)
        elif (nmod == 1) or (nmod == M - 1):
            if M == 2:
                val = 9.0 / (M * 35.0)
            else:
                val = 9.0 / (M * 70.0)

        return val

    def h32Autocorrelation(self, i, j, M):
        if M < self.support:
            raise ValueError(
                "Cannot compute h32Autocorrelation for M < "
                + str(self.support)
            )

        val = 0.0
        if np.abs(i - j) == 1:
            val = -1.0 / ((M - 1.0) * 140.0)
        elif i == j:
            if (i == 0) or (i == M - 1):
                val = 1.0 / ((M - 1.0) * 105.0)
            else:
                val = 2.0 / ((M - 1.0) * 105.0)

        return val

    def h32PeriodicAutocorrelation(self, n, M):
        if M < self.support:
            raise ValueError(
                "Cannot compute h32PeriodicAutocorrelation for M < "
                + str(self.support)
            )

        nmod = np.mod(n, M)
        val = 0.0
        if nmod == 0:
            val = 2.0 / (M * 105.0)
        elif (nmod == 1) or (nmod == M - 1):
            if M == 2:
                val = -1.0 / (M * 70.0)
            else:
                val = -1.0 / (M * 140.0)

        return val

    def h3Crosscorrelation(self, i, j, M):
        if M < self.support:
            raise ValueError(
                "Cannot compute h3Crosscorrelation for M < "
                + str(self.support)
            )

        val = 0.0
        if i - j == 1:
            val = 13.0 / ((M - 1.0) * 420.0)
        elif i - j == -1:
            val = -13.0 / ((M - 1.0) * 420.0)
        elif i == j:
            if i == 0:
                val = 11.0 / ((M - 1.0) * 210.0)
            elif i == M - 1:
                val = -11.0 / ((M - 1.0) * 210.0)

        return val

    def h3PeriodicCrosscorrelation(self, n, M):
        if M < self.support:
            raise ValueError(
                "Cannot compute h3PeriodicCrosscorrelation for M < "
                + str(self.support)
            )

        nmod = np.mod(n, M)
        val = 0.0
        if nmod == 1:
            if M != 2:
                val = 13.0 / (M * 420.0)
        elif nmod == M - 1:
            val = -13.0 / (M * 420.0)

        return val

    def filterPeriodic(self, s):
        raise NotImplementedError(Basis._unimplemented_message)

    def refinementMask(self):
        raise NotImplementedError(Basis._unimplemented_message)

    def filterSymmetric(self, s):
        raise NotImplementedError(Basis._unimplemented_message)


class HE3(Basis):
    def __init__(self, alpha):
        Basis.__init__(self, multigenerator=True, support=2.0)
        self.alpha = alpha

    def value(self, x):
        return np.array([self.he31(x), self.he32(x)])

    def he31(self, x):
        if x >= 0:
            val = self.g1(x)
        else:
            val = self.g1(-x)
        return val

    def g1(self, x):
        val = 0.0
        if 0 <= x <= 1:
            denom = (0.5 * self.alpha * np.cos(0.5 * self.alpha)) - np.sin(
                0.5 * self.alpha
            )
            num = (
                (
                    0.5
                    * (
                        (self.alpha * np.cos(0.5 * self.alpha))
                        - np.sin(0.5 * self.alpha)
                    )
                )
                - (0.5 * self.alpha * np.cos(0.5 * self.alpha) * x)
                - (0.5 * np.sin(0.5 * self.alpha - (self.alpha * x)))
            )
            val = num / denom
        return val

    def he32(self, x):
        if x >= 0:
            val = self.g2(x)
        else:
            val = -1.0 * self.g2(-x)
        return val

    def g2(self, x):
        val = 0.0
        if 0 <= x <= 1:
            denom = (
                (
                    (0.5 * self.alpha * np.cos(0.5 * self.alpha))
                    - np.sin(0.5 * self.alpha)
                )
                * (4.0 * self.alpha)
                * np.sin(0.5 * self.alpha)
            )
            num = (
                -((self.alpha * np.cos(self.alpha)) - np.sin(self.alpha))
                - (
                    2.0
                    * self.alpha
                    * np.sin(0.5 * self.alpha)
                    * np.sin(0.5 * self.alpha)
                    * x
                )
                - (
                    2.0
                    * np.sin(0.5 * self.alpha)
                    * np.cos(self.alpha * (x - 0.5))
                )
                + (self.alpha * np.cos(self.alpha * (x - 1)))
            )
            val = num / denom
        return val

    def firstDerivativeValue(self, x):
        return np.array([self.he31prime(x), self.he32prime(x)])

    def he31prime(self, x):
        if x >= 0:
            val = self.g1prime(x)
        else:
            val = -1.0 * self.g1prime(-x)
        return val

    def g1prime(self, x):
        val = 0.0
        if 0 <= x <= 1:
            denom = (0.5 * self.alpha * np.cos(0.5 * self.alpha)) - np.sin(
                0.5 * self.alpha
            )
            num = -(0.5 * self.alpha * np.cos(0.5 * self.alpha)) + (
                0.5 * self.alpha * np.cos(0.5 * self.alpha - (self.alpha * x))
            )
            val = num / denom
        return val

    def he32prime(self, x):
        if x >= 0:
            val = self.g2prime(x)
        else:
            val = self.g2prime(-x)
        return val

    def g2prime(self, x):
        val = 0.0
        if 0 <= x <= 1:
            denom = (
                (
                    (0.5 * self.alpha * np.cos(0.5 * self.alpha))
                    - np.sin(0.5 * self.alpha)
                )
                * (4.0 * self.alpha)
                * np.sin(0.5 * self.alpha)
            )
            num = (
                -(
                    2.0
                    * self.alpha
                    * np.sin(0.5 * self.alpha)
                    * np.sin(0.5 * self.alpha)
                )
                + (
                    2.0
                    * self.alpha
                    * np.sin(0.5 * self.alpha)
                    * np.sin(self.alpha * (x - 0.5))
                )
                - (self.alpha * self.alpha * np.sin(self.alpha * (x - 1)))
            )
            val = num / denom
        return val

    def secondDerivativeValue(self, x):
        raise RuntimeError("HE3 isn't twice differentiable.")

    def filterPeriodic(self, s):
        raise NotImplementedError(Basis._unimplemented_message)

    def refinementMask(self):
        raise NotImplementedError(Basis._unimplemented_message)

    def filterSymmetric(self, s):
        raise NotImplementedError(Basis._unimplemented_message)


def multinomial(
    maxValue,
    numberOfCoefficiens,
    kArray,
    iteration,
    dilationFactor,
    order,
    mask,
):
    """
    Recursive function to compute the multinomial coefficient of
    (x0+x1+...+xm-1)^N
    This function finds every {k0,...,km-1} such that k0+...+km-1=N
    (cf multinomial theorem on Wikipedia for a detailed explanation)
    """

    if numberOfCoefficiens == 1:
        kArray[iteration] = maxValue

        denominator = 1.0
        degree = 0
        for k in range(0, dilationFactor):
            denominator *= math.factorial(kArray[k])
            degree += k * kArray[k]

        coef = math.factorial(order) / denominator
        mask[int(degree)] += coef / (dilationFactor ** (order - 1))

    else:
        for k in range(0, maxValue + 1):
            kArray[iteration] = k
            multinomial(
                maxValue - k,
                numberOfCoefficiens - 1,
                kArray,
                iteration + 1,
                dilationFactor,
                order,
                mask,
            )

    return


class CardinalExponential(Basis):
    def __init__(self, alpha):
        Basis.__init__(self, multigenerator=False, support=4.0)
        self._alpha = alpha

    def _ba3(self, t):
        if 0 <= t <= 1:
            return -(1 / (self._alpha ** 2)) * (np.cos(self._alpha * t) - 1)
        if 1 < t <= 2:
            return -(2 / (self._alpha ** 2)) * (
                    np.cos(self._alpha) -
                    0.5 * np.cos(self._alpha * (1 - t))
                    - 0.5 * np.cos(self._alpha * (2 - t))
            )
        if 2 < t <= 3:
            return -(1 / (self._alpha ** 2)) * (np.cos(self._alpha * (3 - t)) - 1)
        return 0.0

    def _ba4(self, t):
        if 0 <= t <= 1:
            return -(1 / (self._alpha ** 3)) * (-self._alpha * t +
                                                np.sin(self._alpha * t))
        elif 1 < t <= 2:
            return -(1 / (self._alpha ** 3)) * (
                    self._alpha * (t - 2)
                    + 2 * self._alpha * np.cos(self._alpha) * (t - 1)
                    + 2 * np.sin(self._alpha * (1 - t))
                    + np.sin(self._alpha * (2 - t)))
        if 2 < t <= 3:
            return (1 / (self._alpha ** 3)) * (
                    self._alpha * (t - 2) +
                    2 * self._alpha * np.cos(self._alpha) * (t - 3) +
                    np.sin(self._alpha * (2 - t)) +
                    2 * np.sin(self._alpha * (3 - t))
            )
        if 3 < t <= 4:
            return -(1 / (self._alpha ** 3)) * (
                    self._alpha * (t - 4) + np.sin(self._alpha * (4 - t)))
        return 0.0

    def _ba3Prime(self, t):
        if 0 <= t <= 1:
            return (1 / self._alpha) * np.sin(self._alpha * t)
        if 1 < t <= 2:
            return (1 / self._alpha) * (np.sin(self._alpha * (1 - t)) +
                                        np.sin(self._alpha * (2 - t)))
        if 2 < t <= 3:
            return -(1 / self._alpha) * np.sin(self._alpha * (3 - t))
        return 0.0

    def _ba4Prime(self, t):
        if 0 <= t <= 1:
            return (1 / (self._alpha ** 2)) * (
                    2 * np.sin(0.5 * self._alpha * t) ** 2)
        elif 1 < t <= 2:
            return (1 / (self._alpha ** 2)) * (
                    -2 * np.cos(self._alpha) +
                    np.cos(self._alpha * (t - 2)) +
                    2 * np.cos(self._alpha * (t - 1)) - 1)
        elif 2 < t <= 3:
            return (1 / (self._alpha ** 2)) * (
                    2 * np.cos(self._alpha) -
                    2 * np.cos(self._alpha * (t - 3)) +
                    2 * (np.sin(0.5 * self._alpha * (t - 2)) ** 2))
        elif 3 < t <= 4:
            return -(1 / (self._alpha ** 2)) * (
                    2 * np.sin(0.5 * self._alpha * (4 - t)) ** 2)
        return 0.0

    @property
    def _lambda3(self):
        # Watch out, calculus may be wrong for alphas
        # that are not partitions of 2*pi
        return ((0.5 * self._alpha) ** 2) * \
               (mpmath.csc(0.5 * self._alpha) *
                mpmath.csc(self._alpha) *
                (1 - self._alpha * mpmath.csc(self._alpha))) / \
               (mpmath.sec(0.5 * self._alpha)
                - 0.5 * self._alpha * mpmath.csc(0.5 * self._alpha))

    @property
    def _lambda4(self):
        # Watch out, calculus may be wrong for alphas
        # that are not partitions of 2*pi
        return ((0.5 * self._alpha) ** 3) * \
               (mpmath.sec(0.5 * self._alpha) ** 2) / \
               (np.tan(0.5 * self._alpha) - 0.5 * self._alpha)

    def value(self, t):
        return (self._lambda4 * self._ba4(t + 2) +
                self._lambda3 * (self._ba3(t + 2) + self._ba3(t + 1)))

    def firstDerivativeValue(self, t):
        return (self._lambda4 * self._ba4Prime(t + 2) +
                self._lambda3 * (self._ba3Prime(t + 2) + self._ba3Prime(t + 1)))

    def secondDerivativeValue(self, x):
        raise NotImplementedError(Basis._unimplemented_message)

    def filterSymmetric(self, s):
        raise NotImplementedError(Basis._unimplemented_message)

    def filterPeriodic(self, s):
        raise NotImplementedError(Basis._unimplemented_message)

    def refinementMask(self):
        raise NotImplementedError(Basis._unimplemented_message)
