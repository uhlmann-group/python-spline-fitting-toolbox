import numpy as np
import itertools
import multiprocessing
from . import basis
import scipy.integrate as integrate
import tensorflow as tf
import warnings
import copy
import abc


class SplineSurface(abc.ABC):
    """
    An abstract class for defining spline for 2D surfaces embedded in 3D.

    Such surfaces are parametrised by two parameters:

                        (s, t)  \in  [0, 1]²

    given a two basis function for each dimension and set of
    Ms × Mt control points, where:

      - Ms is the number of parameters along the first dimension described by s.
      - Mt is the number of parameters along the second dimension described by t.

    [1] R. Delgado-Gonzalo, P. Thévenaz, M. Unser,
    Exponential splines and minimal-support bases for curve representation,
    Computer Aided Geometric Design, Volume 29, Issue 2, 2012, Pages 109-128.
    """
    _unimplemented_message = "This method is not implemented."

    def __init__(self, M, basis):
        if len(M) != 2 or len(basis) != 2:
            raise RuntimeError("M and basis must be doublets.")

        if M[0] < basis[0].support or M[1] < basis[1].support:
            raise RuntimeError(
                "Each M must be greater or equal than its spline basis support size."
            )

        self._M = M
        self._basis = basis
        self._halfSupport = (
            self._basis[0].support / 2.0,
            self._basis[1].support / 2.0,
        )
        self._coefs = None

    @property
    def Ms(self):
        return self._M[0]

    @Ms.setter
    def Ms(self, value):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}.Ms")

    @property
    def Mt(self):
        return self._M[1]

    @Mt.setter
    def Mt(self, value):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}.Mt")

    @property
    def _basis_s(self):
        return self._basis[0]

    @_basis_s.setter
    def _basis_s(self, value):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}._basis_s")

    @property
    def _basis_t(self):
        return self._basis[1]

    @_basis_t.setter
    def _basis_t(self, value):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}._basis_t")

    @property
    def coefs(self):
        return self._coefs

    @coefs.setter
    def coefs(self, value):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}.coefs")

    @abc.abstractmethod
    def sample(self, sampling_rates):
        """
        Sample the surface given sampling rates for each dimension.

        @param sampling_rates: tuple of two sampling rates, one for each
        dimension. A simpling rate is a integer which specify the number of
        points to be sampled between two control along one dimension.
        """
        raise NotImplementedError(SplineSurface._unimplemented_message)

    @abc.abstractmethod
    def draw(self, dimensions, cpu_count=1):
        """
        Return a 3D binary image of the model.

        Voxels are equal to 1 if is in inside the model, O otherwise.

        @param dimensions: the shape of the 3D image to render
        @param cpu_count: the number of processes to spawn to render the image.
        """
        raise NotImplementedError(SplineSurface._unimplemented_message)

    @abc.abstractmethod
    def initialise_from_binary_mask(self, binary_mask):
        """
        Set the spline surface using an object 3D binary mask.

        @param binary_mask: the object 3D binary mask.
        """
        raise NotImplementedError(SplineSurface._unimplemented_message)

    @abc.abstractmethod
    def parametersToWorld(self, params, ds=False, dt=False):
        """
        Return a 3D point for a pair of parameter (s, t) \in [0, 1]²

        @param params: the pair of parameter
        @param ds: if True, differentiate w.r.t s
        @param dt: if True, differentiate w.r.t t
        """
        raise NotImplementedError(SplineSurface._unimplemented_message)

    @abc.abstractmethod
    def scale(self, scaling_factor):
        raise NotImplementedError(SplineSurface._unimplemented_message)

    @abc.abstractmethod
    def translate(self, translation_vector):
        raise NotImplementedError(SplineSurface._unimplemented_message)

    @abc.abstractmethod
    def rotate(self, rotation_matrix):
        raise NotImplementedError(SplineSurface._unimplemented_message)


class SplineSphere(SplineSurface):
    """
    A class for defining spline for 2D surfaces embedded in 3D which
    is homeomorphic to the 2-sphere.

    Such a surface is parametrised by two parameters:

                        (s, t)  \in  [0, 1]²

    given a set of (Ms - 2) × Mt control points (excluding the north and south
    poles), where:

      - Ms is the number of parameters along the first dimension described by s.
      - Mt is the number of parameters along the second dimension described by t.

    Notes: Internally, coefficients store control points in a layout for a
    vectorised implementation, allowing better performance as well as support
    for tensors GPU.

    [1] R. Delgado-Gonzalo, P. Thévenaz, M. Unser,
    Exponential splines and minimal-support bases for curve representation,
    Computer Aided Geometric Design, Volume 29, Issue 2, 2012, Pages 109-128.

    [2] R. Delgado-Gonzalo, N. Chenouard and M. Unser,
    Spline-Based Deforming Ellipsoids for Interactive 3D Bioimage Segmentation
    IEEE Transactions on Image Processing, vol. 22, no. 10, pp. 3926-3940,
    Oct. 2013, doi: 10.1109/TIP.2013.2264680.
    """

    # A class dictionary used to map
    # (Ms, Mt, basis function class, s_rate_s, s_rate_t) to phi matrices
    _phi = dict()

    def __init__(self, M, use_cardinal=False):
        if len(M) != 2:
            raise RuntimeError("M must be a doublet.")

        self._PIM = (np.pi / float(M[0] - 1.0), np.pi / float(M[1]))

        self._use_cardinal = use_cardinal

        if self._use_cardinal:
            basis_func = (
                basis.CardinalExponential(2 * (M[0] - 1.0)),
                basis.CardinalExponential(M[1]),
            )
        else:
            basis_func = (
                basis.EM(2 * (M[0] - 1.0), self._PIMs),
                basis.EM(M[1], 2.0 * self._PIMt),
            )

        SplineSurface.__init__(self, M, basis_func)

        self.scale = 1.0 / self._basis_s.firstDerivativeValue(1.0)
        # Scale by (M[0]-1) if model is normalized
        self._scaleM = (
            2.0
            * (1.0 - np.cos(self._PIMs))
            / (np.cos(self._PIMs / 2) - np.cos(3.0 * self._PIMs / 2)),
            2.0
            * (1.0 - np.cos(2.0 * self._PIMt))
            / (np.cos(self._PIMt) - np.cos(3.0 * self._PIMt)),
        )
        
        # _coef stores the control points used for computations for
        # parameterToWorld and for sampling.
        #
        # This_does_ include additional control points.
        # This _does not_ include the north and the south poles.
        #
        #   Real control points correspond to:
        #       (c[l, k])_{k=0, ..., Mt-1} for l in {1, ..., Ms - 2}
        #
        #   Additional control points correspond to:
        #       (c[l, k])_{k=0, ..., Mt-1} for l in {-1, 0, Ms - 1, Ms}
        #
        #
        #  This can be morphed with O(1) time and memory complexities
        #  in a 3D view of shape (Ms + 2, Mt, 3)
        #
        #                   ---------------------------------------------
        #            k = 0                                    k = Mt -1 |
        #              |------------------------------------------|/    |
        #              |   Additional control points for l = -1   |    /|
        #              |   Additional control points for l = 0    |  /  |
        #              |------------------------------------------|/    |
        #     l = 1    |                                          |     |
        #              |                                          |     |
        #              |          Real control points             |     |
        #              |                                          |     |
        #              |                                          |    /|
        #   l = Ms - 2 |                                          |  /  |
        #              |------------------------------------------|/    |
        #              | Additional control points for l = Ms - 1 |    /    z
        #              |   Additional control points for l = Ms   |  /    y
        #              |------------------------------------------|/    x
        #
        self._coefs = np.zeros((self.Mt * (self.Ms + 2), 3))

        # The poles are stored separately
        self._north_pole = None
        self._south_pole = None

        # So there their tangents
        self._tans = np.zeros((4, 3))

        # This keep tracks if the additional control points have been
        # correctly initialised or not.
        # This is set to False, when (non exclusive or):
        #    the north pole is modified, or
        #    the south pole is modified, or
        #    tangents at the pole are modified, or
        #    control points are modified.
        # It is set to True at the end of _initialise_additional_control_points.
        self._is_initialised = False

    def _wrap_parameter(self, t, k, dimension):
        """
        Wrapped an parameter for the given control point along a dimension.

        In the sphere parametrisation, the basis function w.r.t t is made
        Mt-periodic so that continuity is met.

        This method wraps the parameter to reproduce this periodisation.

        In practice, it is being used for t solely.

        @param t: the parameter in [0, Mt] to wrap
        @param k: the index of the control point w.r.t the dimension
        @param dimension: 0 for s, 1 for t
        """
        M = self._M[dimension]
        t_left = t - self._halfSupport[dimension]
        t_right = t + self._halfSupport[dimension]
        if k < t_left <= k + M <= t_right:
            return t - (k + M)
        if t_left <= k - M <= t_right < k:
            return t - (k - M)
        return t - k

    def centroid(self):
        return self.control_points.mean(axis=0)

    def _get(self, k, l):
        """
        A method to access a given control point given the layout of the
        coefficient using the mathematical notation.

        Coefficients are stored in coefs, a array of shape (Mt * (Ms + 2), 3)

        Indices in mathematical notation are:
            k is in {0, ..., Mt -1}.
            l is in {-1, ..., Ms}

        Real control points correspond to: l in {1, ..., Ms - 2}
        Additional control points      to: l in {-1, 0, Ms - 1, Ms}
        """
        return self.Mt * (l + 1) + k

    @property
    def coefs(self):
        if not self._is_initialised:
            self._initialise_additional_control_points()

        return super(SplineSphere, self).coefs

    @property
    def use_cardinal(self):
        return self._use_cardinal

    @use_cardinal.setter
    def use_cardinal(self, value):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}.use_cardinal")

    @property
    def is_initialised(self):
        return self._is_initialised

    @is_initialised.setter
    def is_initialised(self, value):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}.is_initialised")

    @property
    def _PIMs(self):
        return self._PIM[0]

    @_PIMs.setter
    def _PIMs(self, value):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}.PIMs")

    @property
    def _PIMt(self):
        return self._PIM[1]

    @_PIMt.setter
    def _PIMt(self, value):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}.PIMt")

    @property
    def scale_Ms(self):
        """
        The scale of the model w.r.t its first dimension.
        """
        return self._scaleM[0]

    @scale_Ms.setter
    def scale_Ms(self, value):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}.scaleMs")

    @property
    def scale_Mt(self):
        """
        The scale of the model w.r.t its second dimension.
        """
        return self._scaleM[1]

    @scale_Mt.setter
    def scale_Mt(self, value):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}.scaleMt")

    @property
    def tans(self):
        """
        The tangents at the north and the south poles as a (4, 3) array.
        """
        return self._tans

    @tans.setter
    def tans(self, tans):
        values = np.asarray(tans)
        if values.shape != (4, 3):
            raise RuntimeError(f"value must be of shape (4, 3). "
                               f"Current shape: {values.shape}")

        self._tans = tans
        self._is_initialised = False

    @property
    def control_points(self):
        """
        The real control points of the actual model (including
        the north pole and the south poles) as an array of shape
        (Mt * (Ms - 2) + 2, 3).

        The North pole is the first point.
        The South pole is the last point.
        """
        # We exclude the 4 groups of additional virtual control points
        # stacked at the beginning and the end of coefs

        concatenate = tf.concat if tf.is_tensor(self.coefs) else np.concatenate
        control_points = concatenate([
            self.north_pole[None, :],
            self.coefs[2 * self.Mt:-2 * self.Mt],
            self.south_pole[None, :]
            ],
            axis=0
        )

        return control_points

    @control_points.setter
    def control_points(self, points):
        expected_shape = (self.Mt * (self.Ms - 2) + 2, 3)
        if points.shape != expected_shape:
            raise RuntimeError(f"control_points must be of shape {expected_shape}. "
                               f"Current shape: {points.shape}")

        self._north_pole = points[0]
        self._south_pole = points[-1]
        self._coefs[2 * self.Mt:-2 * self.Mt] = points[1:-1]
        if tf.is_tensor(points):
            self._coefs = tf.convert_to_tensor(self._coefs, dtype=points.dtype)

        self._is_initialised = False

    @property
    def north_pole(self):
        return self._north_pole

    @north_pole.setter
    def north_pole(self, point):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}.north_pole")

    @property
    def south_pole(self):
        return self._south_pole

    @south_pole.setter
    def south_pole(self, point):
        raise RuntimeError(f"Can't assign value to {self.__class__.__name__}.south_pole")

    def _compute_phi_uniform_sampling(self, s_rate_s, s_rate_t):
        """
        Compute Phi using a uniform sampling of the parameters given
        sampling rates for the two dimensions.
        """
        Ns = (self.Ms - 1) * s_rate_s + 1
        Nt = self.Mt * s_rate_t
        ss = np.linspace(0, Ns / s_rate_s, num=Ns, endpoint=False)
        ts = np.linspace(0, Nt / s_rate_t, num=Nt, endpoint=False)

        # Both arrays are of shape (Ns, Ms + 2)
        arg_s = np.array([
            [s - l for l in range(-1, self.Ms + 1)]
            for s in ss
        ])
        phi_s = np.vectorize(self._basis_s.value)(arg_s)

        # Both arrays are of shape (Nt, Mt)
        args_t = np.array([
            [self._wrap_parameter(t, k, 1) for k in range(0, self.Mt)]
            for t in ts])
        phi_t = np.vectorize(self._basis_t.value)(args_t)

        # In this case, we make use of the generator separability
        # and we compute Phi as:
        #
        #       Phi_ij = (phi_s)_i (phi_t)_j       for all (i, j)
        #
        # and use some numpy wizardry here by computing the outer product
        # on the last axis of two matrices with einsum.
        #
        # phi is of shape (Ns, Nt, Ms + 2, Mt)
        phi = np.einsum('il, kj->iklj', phi_s, phi_t)

        n_sampled_points = phi.shape[0] * phi.shape[1]
        n_coefs = phi.shape[2] * phi.shape[3]  # i.e. self.coefs.shape[0]

        # phi is now of shape (Ns * Nt, (Ms + 2) * Mt)
        phi = phi.reshape((n_sampled_points, n_coefs))

        return phi

    def _compute_phi_even_sampling(self, N):
        """
        Compute Phi using a "somewhat-even" sampling of the parameters
        relying on the Golden Spiral.

        @param N: the number of points to sample
        """

        # Golden Spiral
        golden_ratio = (1 + 5 ** 0.5) / 2
        x = np.arange(0, N)

        # Angles of evenly sampled points on the sphere
        phi = np.arccos(1 - 2 * (x + 0.5) / N)
        theta = (2 * np.pi * x / golden_ratio) % (2 * np.pi)

        # We map from [0, pi] ⨯ [0, 2pi] to [0, Ms] ⨯ [0, Mt]
        ss = phi / np.pi * (self.Ms - 1)
        ts = theta / (2 * np.pi) * self.Mt

        # Both arrays are of shape (N, Ms + 2)
        arg_s = np.array([
            [s - l for l in range(-1, self.Ms + 1)]
            for s in ss
        ])
        phi_s = np.vectorize(self._basis_s.value)(arg_s)
        assert arg_s.shape == (N, self.Ms + 2)

        # Both arrays are of shape (N, Mt)
        args_t = np.array([
            [self._wrap_parameter(t, k, 1) for k in range(0, self.Mt)]
            for t in ts])
        phi_t = np.vectorize(self._basis_t.value)(args_t)
        assert args_t.shape == (N, self.Mt)

        # In this case, we directly compute a folded version of Phi as:
        #
        #          Phi_(i, k, l) = (phi_s)_{ik} (phi_t)_{il}
        #
        # and use some numpy wizardry as usual with einsum.
        #
        # phi is of shape (N, Ms + 2, Mt)
        phi = np.einsum("ik,il->ikl", phi_s, phi_t)

        n_sampled_points = phi.shape[0]  # i.e N
        n_coefs = phi.shape[1] * phi.shape[2]  # i.e. self.coefs.shape[0]

        # phi is now of shape (N, (Ms + 2) * Mt)
        phi = phi.reshape((n_sampled_points, n_coefs))

        return phi

    def _get_phi(self,  *, s_rate_s=None, s_rate_t=None, N=None):
        """
        Return the phi matrix based on two sampling rates for each dimension
        or based on a number of points to sample.

        @param s_rate_s: the sampling rate on the first dimension
        @param s_rate_t: the sampling rate on the second dimension
        @param N: the number of points to sample
        """

        key = (self.Ms, self.Mt, self._basis_s.__class__,
               s_rate_s, s_rate_t, N)
        phi = SplineSphere._phi.get(key, None)
        if phi is None:
            # We compute it once and we store it
            if s_rate_t is not None and s_rate_t is not None:
                phi = self._compute_phi_uniform_sampling(s_rate_s, s_rate_t)
            else:
                phi = self._compute_phi_even_sampling(N)

            # Support for GPU computations
            if tf.is_tensor(self._coefs):
                phi = tf.convert_to_tensor(phi, dtype=self._coefs.dtype)

            SplineSphere._phi[key] = phi
        return phi

    def _initialise_additional_control_points(self):
        """
        Before performing any sampling, the additional (virtual) control points

            (c[l, k])_{k=0, ..., Mt-1} for l in {-1, 0, Ms - 1, Ms}

        need to be computed as their value depends on the poles' and their
        tangents'.
        """
        if self._coefs is None:
            raise RuntimeError("This model doesn't have any coefficients.")

        if self._north_pole is None:
            raise RuntimeError("North pole needs to be set.")

        if self._south_pole is None:
            raise RuntimeError("North pole needs to be set.")

        if self._tans is None:
            raise RuntimeError("The tangents at the poles need to be set.")

        # North tangent plane
        T1N = self._tans[0] - self.north_pole
        T2N = self._tans[1] - self.north_pole

        # South tangent plane
        T1S = self._tans[2] - self.south_pole
        T2S = self._tans[3] - self.south_pole

        v0 = self._basis_s.value(0.0)
        v1 = self._basis_s.value(1.0)

        # Reshaping it in a 3D view of shape (Ms + 2, Mt, 3)
        # so that it's possible to assign additional control points more easily

        # We explicitly cast it to a numpy array as self._coefs
        # as it can be stored as a Tensorflow Tensor which doesn't support
        # this indexing.
        coefs = np.array(self._coefs).reshape(((self.Ms + 2), self.Mt, 3))

        k = np.arange(self.Mt)
        phi = 2 * self._PIMt * k
        cos_phi = np.cos(phi)
        sin_phi = np.sin(phi)

        # Note: mind the shift in the indexing, that is,
        # for l in {-1, 0, Ms - 1, Ms}
        #
        #        (c[l, k])_{k=0, ..., Mt-1}
        #
        #  corresponds to
        #
        #               coefs[l + 1, :]

        if self._use_cardinal:
            # (c[-1, k])_{k=0, ..., Mt-1}
            coefs[0, :] = (
                    coefs[2, :] +
                    self.scale / (self.Ms - 1) *
                    (np.outer(cos_phi, T1N) + np.outer(sin_phi, T2N))
            )

            # (c[0, k])_{k=0, ..., Mt-1}
            coefs[1, :] = self._coefs[self._get(k=0, l=self.Ms-3)]

            # (c[Ms, k])_{k=0, ..., Mt-1}
            coefs[self.Ms + 1, :] = (
                    coefs[self.Ms - 1, :] +
                    self.scale / (self.Ms - 1) *
                    (np.outer(cos_phi, T1S) + np.outer(sin_phi, T2S))
            )

            # (c[Ms - 1, k])_{k=0, ..., Mt-1}
            coefs[self.Ms, :] = self._coefs[self._get(k=1, l=self.Ms-3)]
        else:
            # (c[-1, k])_{k=0, ..., Mt-1}
            coefs[0, :] = (
                    coefs[2, :] +
                    self.scale * self.scale_Mt / (self.Ms - 1) *
                    (np.outer(cos_phi, T1N) + np.outer(sin_phi, T2N))
            )

            # (c[0, k])_{k=0, ..., Mt-1}
            coefs[1, :] = (self.north_pole - v1 * (
                    coefs[0, :] + coefs[2, :])) / v0

            # (c[Ms, k])_{k=0, ..., Mt-1}
            coefs[self.Ms + 1, :] = (
                    coefs[self.Ms - 1, :] +
                    self.scale * self.scale_Mt / (self.Ms - 1) *
                    (np.outer(cos_phi, T1S) + np.outer(sin_phi, T2S))
            )

            # (c[Ms - 1, k])_{k=0, ..., Mt-1}
            coefs[self.Ms, :] = (self.south_pole - v1 * (
                    coefs[self.Ms + 1, :] + coefs[self.Ms - 1, :])) / v0

        # Reusing the original layout
        reshaped_coefs = coefs.reshape(((self.Ms + 2) * self.Mt, 3))

        # GPU support
        if tf.is_tensor(self._coefs):
            self._coefs = tf.convert_to_tensor(reshaped_coefs,
                                               dtype=self._coefs.dtype)
        else:
            self._coefs = reshaped_coefs

        self._is_initialised = True

    def _point_expression(self, theta, phi):
        if self._use_cardinal:
            return np.array([
                np.sin(theta) * np.cos(phi),
                np.sin(theta) * np.sin(phi),
                np.cos(theta),
            ])

        return np.array([
            self.scale_Ms * self.scale_Mt * np.sin(theta) * np.cos(phi),
            self.scale_Ms * self.scale_Mt * np.sin(theta) * np.sin(phi),
            self.scale_Ms * np.cos(theta),
        ])

    def initialize_sphere(self, radius, center):
        for k in range(0, self.Mt):
            for l in range(1, self.Ms - 1):
                theta = self._PIMs * l
                phi = 2.0 * self._PIMt * k

                vect = self._point_expression(theta, phi)

                self._coefs[self._get(k, l)] = center + (radius * vect)

        # North pole
        self._north_pole = center + np.array([0, 0, radius])
        # South pole
        self._south_pole = center + np.array([0, 0, -radius])

        # North tangent plane
        self._tans[0] = self.north_pole + np.array([np.pi * radius, 0, 0])
        self._tans[1] = self.north_pole + np.array([0, np.pi * radius, 0])
        # South tangent plane
        self._tans[2] = self.south_pole + np.array([np.pi * radius, 0, 0])
        self._tans[3] = self.south_pole + np.array([0, np.pi * radius, 0])

        self._initialise_additional_control_points()
        return self

    def initialize_from_points(self, sample_points, sampling_rates):
        """
        Setup the spline from a set of points on the surface.

        This internally find the control points by solving a least squares
        problem.

        @param sample_points: array of shape (n_points, 3).
        Contains the points on the samples to interpolate.

        @param sampling_rates: tuple of two sampling rates, one for each
        dimension. A simpling rate is a integer which specify the number of
        points to be sampled between two control along one dimension.
        """
        if len(sampling_rates) != 2:
            raise RuntimeError("sampling_rate must be a doublet.")

        s_rate_s, s_rate_t = sampling_rates

        n_points = self.Mt * s_rate_t * (((self.Ms - 1) * s_rate_s) + 1)

        if sample_points.shape != (n_points, 3):
            raise RuntimeError(
                f"sample_points must be of shape "
                f"(Mt * s_rate_t * (((Ms - 1) * s_rate_s) + 1), 3) = "
                f"({n_points}, 3).\n "
                f"Current shape {sample_points.shape}")

        # We solve an inverse problem here to find the original control_points
        phi = self._get_phi(s_rate_s=s_rate_s, s_rate_t=s_rate_t)
        control_points, _, _, _ = np.linalg.lstsq(phi, sample_points, rcond=None)

        coefs = np.zeros((self.Mt, self.Ms + 2, 3))
        for l in range(-1, self.Ms + 1):
            for k in range(0, self.Mt):
                kp = k + (self.Mt * (l + 1))
                self._coefs[self._get(k, l)] = coefs[k, l + 1] = control_points[kp]

        # GPU support
        if tf.is_tensor(sample_points):
            self._coefs = tf.convert_to_tensor(self._coefs,
                                               dtype=self._coefs.dtype)

        vs0 = self._basis_s.value(0.0)
        vs1 = self._basis_s.value(1.0)

        # Poles and tangent vectors
        cN = (vs0 * coefs[:, 1]
              + vs1 * (coefs[:, 0] + coefs[:, 2])).sum(axis=0)
        cS = (vs0 * coefs[:, self.Ms] +
              vs1 * (coefs[:, self.Ms -1] + coefs[:, self.Ms + 1])).sum(axis=0)

        shifted_coefs = np.roll(coefs, shift=-1, axis=0)
        ks = np.arange(self.Mt)[:, np.newaxis]
        ks_p1 = np.roll(ks, shift=-1, axis=0)
        phi = 2.0 * self._PIMt * ks
        shifted_phi = 2.0 * self._PIMt * ks_p1

        sin_phi = np.sin(phi)
        cos_phi = np.cos(phi)
        sin_shifted_phi = np.sin(shifted_phi)
        cos_shifted_phi = np.cos(shifted_phi)
        denominator = self.scale_Mt * (
            (sin_shifted_phi * cos_phi) - (cos_shifted_phi * sin_phi)
        )

        delta1 = coefs[:, 0] - coefs[:, 2]
        delta2 = shifted_coefs[:, 0] - shifted_coefs[:, 2]

        delta3 = coefs[:, self.Ms + 1] - coefs[:, self.Ms - 1]
        delta4 = shifted_coefs[:, self.Ms + 1] - shifted_coefs[:, self.Ms - 1]

        T1N = ((sin_shifted_phi * delta1 - sin_phi * delta2) / denominator).sum(axis=0)
        T2N = ((cos_phi * delta2 - cos_shifted_phi * delta1) / denominator).sum(axis=0)

        T1S = ((sin_shifted_phi * delta3 - sin_phi * delta4) / denominator).sum(axis=0)
        T2S = ((cos_phi * delta4 - cos_shifted_phi * delta3) / denominator).sum(axis=0)

        # North pole
        self._north_pole = cN / float(self.Mt)
        # South pole
        self._south_pole = cS / float(self.Mt)

        fac = (self.Ms - 1.0) / (self.scale * float(self.Mt))

        # North tangent plane
        self._tans[0] = self.north_pole + T1N * fac
        self._tans[1] = self.north_pole + T2N * fac
        # South tangent plane
        self._tans[2] = self.south_pole + T1S * fac
        self._tans[3] = self.south_pole + T2S * fac

        self._initialise_additional_control_points()
        return self

    def scale(self, scaling_factor):
        centroid = self.centroid()
        centered_coefs = self.coefs - centroid
        self._coefs = centroid + scaling_factor * centered_coefs

        centered_tans = self.tans - centroid
        self._tans = centroid + scaling_factor * centered_tans
        self._initialise_additional_control_points()

    def rotate(self, rotation_matrix):
        # This is a rotation of the whole cartesian space,
        # make sure the model is centered if you want to rotate it on itself.
        self._coefs = np.matmul(rotation_matrix, self.coefs)
        self._tans = np.matmul(rotation_matrix, self.tans)
        self._initialise_additional_control_points()

    def translate(self, translation_vector):
        self._coefs += translation_vector
        self._tans += translation_vector
        self._initialise_additional_control_points()

    def _ddst(self, s, t):
        return (self._basis_s.firstDerivativeValue(s)
                * self._basis_t.firstDerivativeValue(t) /
                (2 * self._PIMs * self._PIMt))

    def _ds(self, s, t):
        return (self._basis_s.firstDerivativeValue(s)
                * self._basis_t.value(t) / self._PIMs)

    def _dt(self, s, t):
        return (self._basis_s.value(s)
                * self._basis_t.firstDerivativeValue(t) / (2 * self._PIMt))

    def _val(self, s, t):
        return self._basis_s.value(s) * self._basis_t.value(t)

    def parametersToWorld(self, params, ds=False, dt=False):
        if not self._is_initialised:
            self._initialise_additional_control_points()

        s, t = params
        if ds and dt:
            func = self._ddst
        elif ds:
            func = self._ds
        elif dt:
            func = self._dt
        else:
            func = self._val

        phi = np.array([
            func(s - l, self._wrap_parameter(t, k, 1))
            for l in range(-1, self.Ms + 1)
            for k in range(0, self.Mt)
        ])

        return phi @ self.coefs

    def sample(self,  *, sampling_rates=None, N=None):
        if sampling_rates is None and N is None:
            raise RuntimeError("sampling_rates must be provided as a doublet "
                               "or N must be provided")

        if not self._is_initialised:
            self._initialise_additional_control_points()

        s_rate_s, s_rate_t = (
            sampling_rates
            if sampling_rates is not None else (None, None)
        )
        phi = self._get_phi(s_rate_s=s_rate_s, s_rate_t=s_rate_t, N=N)
        contour_points = phi @ self.coefs
        return contour_points

    def draw(self, dimensions, cpu_count=1):
        if not self._is_initialised:
            self._initialise_additional_control_points()

        warnings.warn("draw() will take ages, go get yourself a coffee.")

        if len(dimensions) != 3:
            raise RuntimeError("dimensions must be a triplet.")

        xvals = range(dimensions[2])
        yvals = range(dimensions[1])
        zvals = range(dimensions[0])
        pointsList = list(itertools.product(xvals, yvals, zvals))

        cpu_count = np.min((cpu_count, multiprocessing.cpu_count()))
        if cpu_count == 1:
            vals = [self.isInside(p) for p in pointsList]
            vals = np.asarray(vals)
        else:
            pool = multiprocessing.Pool(cpu_count)
            res = pool.map(self.isInside, pointsList)
            vals = np.stack(res)

        volume = np.zeros(dimensions, dtype=np.int8)
        for i in range(0, len(pointsList)):
            p = pointsList[i]
            volume[p[2], p[1], p[0]] = int(255 * vals[i])

        return volume

    def windingNumber(self, s, t):
        sigma = self.parametersToWorld((s, t))
        dsigmads = self._PIMs * self.parametersToWorld((s, t), ds=True, dt=False)
        dsigmadt = 2.0 * self._PIMt * self.parametersToWorld((s, t), ds=False, dt=True)

        sigma3 = np.linalg.norm(sigma) ** 3
        val = (1.0 / sigma3) * (
            sigma[0] * (dsigmads[1] * dsigmadt[2] - dsigmads[2] * dsigmadt[1])
            + sigma[1] * (dsigmads[2] * dsigmadt[0] - dsigmads[0] * dsigmadt[2])
            + sigma[2] * (dsigmads[0] * dsigmadt[1] - dsigmads[1] * dsigmadt[0])
        )
        return val

    def isInside(self, point):
        original_coefs = copy.deepcopy(self.coefs)
        original_tans = copy.deepcopy(self._tans)
        self._coefs = original_coefs - point
        self._tans = original_tans - point
        self._initialise_additional_control_points()

        f = lambda s, t: self.windingNumber(s, t)
        res = integrate.dblquad(
            f, 0.0, self.Mt, lambda t: 0.0, lambda t: self.Ms - 1.0
        )

        self._coefs = original_coefs
        self._tans = original_tans
        self._initialise_additional_control_points()

        val = res[0]
        if np.abs(val - 4.0 * np.pi) < 1e-6:
            return 1
        elif np.abs(val - 2.0 * np.pi) < 1e-6:
            return 0.5
        else:
            return 0

    def initialise_from_binary_mask(self, binary_mask, sampling_rate=(20, 20)):
        """
        TODO: to test

        """

        def _ray_trace(volume, p0, p1):
            i0 = np.asarray(p0)
            i1 = np.asarray(p1)

            if np.sum((i0 - i1) ** 2) < 0.25:
                return i0

            if np.any(i1 < 0) or np.any(i1 >= volume.shape[::-1]):
                i1 = _edge_trace(volume, i0, i1)

            im = np.asarray((i0 + i1) / 2)
            label0 = volume[tuple(i0.astype(int)[::-1])]
            if label0 != 1:
                return i0

            labelm = volume[tuple(im.astype(int)[::-1])]
            if labelm == label0:
                return _ray_trace(volume, im, i1)
            else:
                return _ray_trace(volume, i0, im)

        def _edge_trace(volume, i0, i1):
            inf = 0
            sup = 1
            step = 1.0 / max(volume.shape)
            while (sup - inf) > step:
                factor = (inf + sup) / 2.0
                ii = i0 + factor * (i1 - i0)
                ii[np.logical_or(ii < 0, ii >= volume.shape[::-1])] = np.nan
                try:
                    _ = volume[tuple(ii.astype(int)[::-1])]
                    inf = factor
                except:
                    sup = factor
            ii = i0 + inf * (i1 - i0)
            return ii

        indices = np.where(binary_mask == 1)[::-1]
        center = np.asarray(indices).mean(axis=1)

        dS = np.array([0, 0, 1])
        dT = np.array([[1, 0, 0], [0, 1, 0]])

        poles = np.stack((_ray_trace(binary_mask, center,
                                     center - (max(binary_mask.shape) * dS)),
                          _ray_trace(binary_mask, center,
                                     center + (max(binary_mask.shape) * dS))))
        distances = np.sqrt(np.sum((poles - center) ** 2, axis=1))
        L = distances[0]
        Lsum = np.sum(distances)

        s0 = center - L * dS

        J = ((self.Ms - 1) * sampling_rate[0]) + 1
        I = self.Mt * sampling_rate[1]

        alphas = np.asarray(range(I)) * 2.0 * np.pi / I
        if np.any(np.abs(np.cross(dT[0], dT[1]) - dS) > 1e-8):
            alphas = -alphas

        sample_points = np.zeros((I * J, 3))
        for j in range(0, J):
            beta = j * np.pi / (J - 1)
            c = s0 + (dS * ((Lsum / 2.0) - ((Lsum / 2.0) * np.cos(beta))))

            for i in range(0, I):
                ip = i + I * j
                if (j == 0) or (j == J - 1):
                    sample_points[ip] = c
                else:
                    v = (np.cos(alphas[i]) * dT[0]) + (np.sin(alphas[i]) * dT[1])
                    sample_points[ip] = _ray_trace(binary_mask, c,
                                                   c + (max(binary_mask.shape) * v))

        self.initialize_from_points(sample_points, sampling_rate)
        return self
